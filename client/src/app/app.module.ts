//modules
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtModule } from "@auth0/angular-jwt";
import { ToastrModule } from 'ngx-toastr';
import { CoreModule } from './shared/modules/core.module';
import { AppRoutingModule } from './app-routing.module';
import { SystemModule } from './system-page/system-layout/modules/system.module';

//components
import { AppComponent } from './app.component';
import { AuthModule } from './auth-page/modules/auth.module';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';

//language
import { registerLocaleData } from '@angular/common';
import ruLocale from '@angular/common/locales/ru';

//auth
import { AuthInterceptor } from './auth-page/interceptors/auth.interceptor';
import { tokenGetter } from './auth-page/handlers/auth.handlers';

//env
import { environment } from '@env/environment';

registerLocaleData(ruLocale, 'ru');

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AuthModule,
    SystemModule,
    AppRoutingModule,
    CoreModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter,
        whitelistedDomains: [environment.API_URL],
        blacklistedRoutes: [ new RegExp(/api[/]auth[/]/, 'i') ]
      }
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
