import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, Subject } from 'rxjs';
import { catchError, switchMap, retryWhen, scan, delay } from 'rxjs/operators';
import { environment } from '@env/environment';
import { AuthService } from '../services/auth.service';
import { TAuthTokens } from '../types/auth.types';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from '../services/login.service';
import { NSTOASTR } from 'src/app/shared/namespaces/shared.namespaces';
import { StorageService } from 'src/app/shared/services/storage.service';
import { NSAUTH } from '../namespaces/auth.namespaces';
import { tokenSetter } from '../handlers/auth.handlers';
import { prefix, suffix } from 'src/app/shared/handlers/shared.handlers';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    isRefresh: boolean = false;
    refresh$: Subject<HttpHeaders> = new Subject();

    constructor(
        private toastr: ToastrService,
        private storage: StorageService,
        private loginService: LoginService,
        private authService: AuthService,
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { API_URL } = environment;
        const url: string = req.url.replace('@', suffix(API_URL, '/api'));
        const clone: HttpRequest<any> = req.clone({ url });

        return next.handle(clone).pipe(
            catchError((err: HttpErrorResponse) => {
                const message: string = err?.error?.message || err?.message || String(err);

                switch(err.status) {
                    case 400: {
                       return this.refreshTokens(req, next, url);
                    }

                    case 401: {
                        this.loginService.logout();
                        break;
                    }
                }

                this.toastr.error(message, NSTOASTR.ERROR);

                return throwError(err);
            }),
            retryWhen((errors) => {
                return errors.pipe(
                    delay(1000),
                    scan((accum, error) => {
                        if(accum === 5 || error.status !== 500) {
                            throw 'server error';
                        } 

                        return accum + 1;
                    }, 0)
                )
            })
        )
    }

    refreshTokens(req: HttpRequest<any>, next: HttpHandler, url: string): Observable<any> {
          if(!this.isRefresh) {
            this.isRefresh = true;
            const { refresh_token }: TAuthTokens = this.storage.getItem(NSAUTH.TOKENS_KEY);
            
            return this.authService.refreshTokens(refresh_token).pipe(
                switchMap((tokens: TAuthTokens) => {
                    tokenSetter(tokens);
                    const Authorization: string = prefix(tokens.access_token, 'Bearer ');
                    const headers: HttpHeaders = new HttpHeaders({ Authorization });
                    const clone = req.clone({ url, headers });

                    this.refresh$.next(headers);
                    this.isRefresh = false;

                    return next.handle(clone);
                }),
                catchError((err: HttpErrorResponse) => {
                    switch(err.status) {
                        case 401: {
                            this.loginService.logout();
                            break;
                        }

                        default: {
                            this.toastr.error(err?.error?.message || err, NSTOASTR.ERROR);
                        }
                    }

                    this.isRefresh = false;
                    this.refresh$.next(null);

                    return throwError(err);
                })
            )
        }

        return this.refresh$.pipe(
            switchMap((headers: HttpHeaders) => {

                if(headers) {
                    return next.handle(req.clone({ url, headers }));
                }
                
                return throwError('request error')
            })
        )
    }
}