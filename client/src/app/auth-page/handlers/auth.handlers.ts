import { TAuthTokens } from '../types/auth.types';
import { NSAUTH } from '../namespaces/auth.namespaces';

export function tokenGetter(): string {
    const str: string = window.localStorage.getItem(NSAUTH.TOKENS_KEY);
    const tokens: TAuthTokens = JSON.parse(str);

    return tokens?.access_token;
}

export function tokenSetter(tokens: TAuthTokens): void {
    window.localStorage.setItem(NSAUTH.TOKENS_KEY, JSON.stringify(tokens))
}

