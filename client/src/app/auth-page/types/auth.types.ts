import { IUser } from 'src/app/shared/user/interfaces/IUser'

export type TDifficultPasswordOption = {
    message: string,
    reg: RegExp,
    status: TDifficultStatus,
    title: string
}

export type TDifficultStatus = 'low' | 'middle' | 'high' | 'extra';

export type TAuthTokens = {
    access_token: string;
    refresh_token: string;
}

export type TLoginUser = Pick<IUser, 'Email' | 'Password'>;