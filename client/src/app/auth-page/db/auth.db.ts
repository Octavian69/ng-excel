export const AuthDB = {
    registration: {
        header: ['Р','е','г','и','с','т','р','а','ц','и','я'],
        extraLengthCss: { color: '#fff' },
        difficultPasswordOptions: [
            { message: 'Одну цифру.', status: 'low', title: 'Низкая', reg: new RegExp('[0-9]') },
            { message: 'Одну прописную букву.', status: 'middle', title: 'Средняя', reg: new RegExp('[A-ZА-ЯЁ]') },
            { message: 'Одну строчную букву.', status: 'high', title: 'Высокая', reg: new RegExp('[a-zа-яё]') },
            { message: 'Один спецсимвол.', status: 'extra', title: 'Максимальная', reg: new RegExp('[!@#$%&*|/\\~]') }
        ]
    }
}

export type TAuthDB = typeof AuthDB;