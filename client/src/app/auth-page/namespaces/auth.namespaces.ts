import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';
import { ILength } from 'src/app/shared/interfaces/shared/ILength';

export namespace NSAUTH {
    export const AUTH_KEY = 'isLogged';
    export const TOKENS_KEY = 'tokens';
}

export namespace NSAUTH_LENGTH {
    export const FORM: ISimple<ILength> = {
        UserName: {
            min: 2,
            max: 30
        },
        Password: {
            min: 7,
            max: 30
        }
    }
}