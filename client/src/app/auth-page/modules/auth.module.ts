import { NgModule } from '@angular/core';
import { CoreModule } from 'src/app/shared/modules/core.module';
import { AntModule } from 'src/app/shared/modules/ant/modules/ant.module';
import { AuthRoutingModule } from './auth-routing.module';
import { FormModule } from 'src/app/shared/form/modules/form.module';

import { AuthService } from '../services/auth.service';
import { LoginService } from '../services/login.service';
import { AuthViewService } from '../services/auth.view.service';
import { AuthGuard } from '../guards/auth.guard';

import { AuthPageComponent } from '../components/auth-page/auth-page.component';
import { LoginComponent } from '../components/login/login.component';
import { RegistrationComponent } from '../components/registration/registration.component';
import { ComponentsModule } from 'src/app/shared/modules/components.module';

@NgModule({
    declarations: [
        AuthPageComponent,
        LoginComponent,
        RegistrationComponent
    ],
    imports: [
        CoreModule,
        AntModule,
        AuthRoutingModule,
        FormModule,
        ComponentsModule
    ],
    providers: [
        AuthService,
        AuthViewService,
        LoginService,
        AuthGuard
    ]
})
export class AuthModule {}