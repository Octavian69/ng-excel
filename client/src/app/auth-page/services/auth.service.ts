import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/shared/user/models/User';
import { IUser } from 'src/app/shared/user/interfaces/IUser';
import { TAuthTokens, TLoginUser } from '../types/auth.types';
import { IMessage } from 'src/app/shared/interfaces/services/IMessage';

@Injectable()
export class AuthService {
    constructor(
        private http: HttpClient
    ) {}

    login(user: TLoginUser): Observable<TAuthTokens> {
        return this.http.post<TAuthTokens>(`@/auth/login`, user);
    }

    registration(user: User): Observable<IMessage> {
        return this.http.post<IMessage>(`@/auth/registration`, user)
    }

    getUserByEmail(Email: string): Observable<IUser> {
        return this.http.post<IUser>('@/auth/get-by-email', { Email });
    }

    refreshTokens(refreshToken: string): Observable<TAuthTokens> {
        return this.http.put<TAuthTokens>(`@/auth/refresh-tokens`, { refreshToken })
    }
}