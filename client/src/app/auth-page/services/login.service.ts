import { Injectable } from "@angular/core";
import { StorageService } from 'src/app/shared/services/storage.service';
import { NSAUTH } from '../namespaces/auth.namespaces';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { User } from 'src/app/shared/user/models/User';
import { TAuthTokens, TLoginUser } from '../types/auth.types';
import { tokenSetter } from '../handlers/auth.handlers';
import { IMessage } from 'src/app/shared/interfaces/services/IMessage';
import { ToastrService } from 'ngx-toastr';
import { NSTOASTR } from 'src/app/shared/namespaces/shared.namespaces';
import { AuthDataFlags } from '../flags/auth.data.flags';
import { TAuthAction } from '../types/auth.services.types';
import { DataServiceManager } from 'src/app/shared/managers/models/DataServiceManager';



@Injectable()
export class LoginService extends DataServiceManager<AuthDataFlags, TAuthAction> {
    constructor(
        private router: Router,
        private toastr: ToastrService,
        private storage: StorageService,
        private authService: AuthService
    ) {
        super(new AuthDataFlags);
    }

    setLogged(flag: boolean): void {
        this.storage.setItem(NSAUTH.AUTH_KEY, flag);
    }

    isLogged(): boolean {
        return this.storage.getItem(NSAUTH.AUTH_KEY);
    }
    
    login(user: TLoginUser): void {
        this.setFlag('isLoginReq', true);

        const next = (tokens: TAuthTokens) => {
            tokenSetter(tokens);
            this.setLogged(true);
            this.router.navigate(['/dashboard']);
            this.setFlag('isLoginReq', false);
        }

        const error = (err) => {
            this.setFlag('isLoginReq', false);
        }

        this.authService.login(user).subscribe({ next, error });
    }

    registration(user: User): void {
        this.setFlag('isRegistrationReq', true);

        const next = ({ message }: IMessage) => {
            this.toastr.success(message, NSTOASTR.SUCCESS);
            this.router.navigate(['/login']);
            this.emit('registration');
            this.setFlag('isRegistrationReq', false);
        }

        const error = (err) => {
            this.setFlag('isRegistrationReq', false);
        }

        this.authService.registration(user).subscribe({ next, error });
    }

    logout(): void {
        this.storage.clear();
        this.router.navigate(['/login']);
    }
}