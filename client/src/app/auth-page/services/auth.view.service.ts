import { Injectable } from '@angular/core';
import { AuthViewFlags } from '../flags/auth.view.flags';
import { AuthDB, TAuthDB } from '../db/auth.db';
import { ViewServiceManager } from 'src/app/shared/managers/models/ViewServiceManager';

@Injectable()
export class AuthViewService extends ViewServiceManager<AuthViewFlags, null, TAuthDB> {
    constructor() {
        super(new AuthViewFlags, null, AuthDB);
    }
}