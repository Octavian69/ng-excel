import { Injectable } from '@angular/core';
import { ServicesModule } from '../../shared/modules/services.module';
import { JwtHelperService } from '@auth0/angular-jwt';
import { StorageService } from '../../shared/services/storage.service';
import { TAuthTokens } from 'src/app/auth-page/types/auth.types';

@Injectable({ 
    providedIn: ServicesModule
})
export class JWTService {

    constructor(
        private jwtHelper: JwtHelperService,
        private storage: StorageService
    ) {}

    id(): string {
        const { access_token }: TAuthTokens = this.storage.getItem('tokens');

        if(access_token) {
            const { _id } = this.jwtHelper.decodeToken(access_token);

            return _id;
        }

        throw new Error('token not found');
    }
}