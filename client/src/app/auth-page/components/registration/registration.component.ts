import { Component, OnInit, EventEmitter, Output, Input, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ILength } from 'src/app/shared/interfaces/shared/ILength';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';
import { TVisibility } from 'src/app/shared/types/shared.types';
import { prefix, suffix } from 'src/app/shared/handlers/shared.handlers';
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { AuthViewService } from '../../services/auth.view.service';
import { AuthService } from '../../services/auth.service';
import { AppValidator } from 'src/app/shared/form/classes/AppValidator';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/shared/user/interfaces/IUser';
import { User } from 'src/app/shared/user/models/User';
import { IAction } from 'src/app/shared/interfaces/shared/IAction';
import { filter, takeUntil } from 'rxjs/operators';
import { TDebounceValidorParams } from 'src/app/shared/form/types/form.types';
import { TAuthAction } from '../../types/auth.services.types';
import { OnInitEmitterStream } from 'src/app/shared/interfaces/hooks/OnInitEmitterStream';
import { ANFade, ANStateScale } from 'src/app/shared/animations/animations';
import { TDifficultPasswordOption, TDifficultStatus } from '../../types/auth.types';
import { OnInitMetadata } from 'src/app/shared/interfaces/hooks/OnInitMetadata';
import { UnsubscribeManager } from 'src/app/shared/managers/models/UnsubscribeManager';

@Component({
  selector: 'excel-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANFade(), ANStateScale()]
})
export class RegistrationComponent extends UnsubscribeManager implements 
  OnInit, 
  OnInitMetadata,
  OnInitEmitterStream<TAuthAction>, 
  OnDestroy 
{

  form: FormGroup;
  header: string[];
  extraLengthCss: string[];
  difficultPasswordOptions: TDifficultPasswordOption[];

  @Input() emitter$: Observable<IAction<TAuthAction>>
  @Input() length: ISimple<ILength>;
  @Input() request: boolean = false;
  @Input() verify: boolean = false;
  @Input() isShow: boolean = false;
  @Output('show') _show = new EventEmitter<void>();
  @Output('hide') _hide = new EventEmitter<void>();
  @Output('registration') _registration = new EventEmitter<IUser>();

  constructor(
    private fb: FormBuilder,
    private viewService: AuthViewService,
    private authService: AuthService
  ) { super() }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  ngOnInitMetadata(): void {
    this.header = this.viewService.getDataFromDB(['registration', 'header']);
    this.extraLengthCss = this.viewService.getDataFromDB(['registration', 'extraLengthCss'])
    this.difficultPasswordOptions = this.viewService.getDataFromDB([
      'registration', 
      'difficultPasswordOptions'], false);
      
  }

  init(): void {
    this.ngOnInitEmiterStream();
    this.ngOnInitMetadata();
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      Email: [null, 
      [Validators.required, Validators.email],
      [AppValidator.asyncDebounce(this.getUserByEmail())]
    ],
      Password: [null, [
        Validators.required, 
        Validators.minLength(this.length.Password.min),
        Validators.maxLength(this.length.Password.max),
      ]],
      ConfirmPassword: [null, [Validators.required]]
    })
  }

  ngOnInitEmiterStream(): void {
    this.emitter$.pipe(
      filter(({ type }) => type === 'registration'),
      takeUntil(this.untilDestoyed())
    ).subscribe(_ => {
      this.form.reset();
      this.show('hide');
    })
  }

  getUserByEmail(): TDebounceValidorParams<string, IUser> {
    const handler = (user: IUser): ValidationErrors => {
      return user ? {existEmail: true} : null;
    };

    return [
      this.authService.getUserByEmail.bind(this.authService),
      handler 
    ]
  }

  getDifficultStatus(): TDifficultStatus {
    const percent: number = this.getDifficultPercent();

    switch(percent) {
      case 25: return 'low';
      case 50: return 'middle';
      case 75: return 'high';
      case 100: return 'extra';
    }
  }

  getDifficultPercent(): number {
    const { value } = this.form.get('Password');
    const percent: number = this.difficultPasswordOptions
    .reduce((accum, { reg }) => {
      if(reg.test(value)) accum += 25;

      return accum;
    }, 0)

    return percent;
  }

  getDifficultStyles(isIcon: boolean = false): ISimple<boolean> {
    const status: TDifficultStatus = this.getDifficultStatus();
    const className: string = isIcon ? suffix(status, '-icon') :  status;

    return { [className]: true };
  }

  getDifficultStatusTitle(): string {
    const currentStatus: TDifficultStatus = this.getDifficultStatus();
    const option: TDifficultPasswordOption = this.difficultPasswordOptions
      .find(({ status}) => status === currentStatus);
    return  option?.title;
  }

  trackByFn(idx: number): number {
    return idx;
  }

  show(type: TVisibility): void {
    const method = prefix(type, '_');
    this[method].emit();
  }

  isDisabledForm(): boolean {
    return this.form.invalid || this.request || !this.isEqualPasswords();
  }

  isShowDifficlutIcon(): boolean {
    const { valid } = this.form.get('Password');

    return valid;
  }

  isEqualPasswords(): boolean {
      const confirmValue: string = this.form.get('ConfirmPassword').value;
      const { value } = this.form.get('Password');
      const isEqual: boolean = Object.is(value, confirmValue);
  
      return isEqual;
  } 

  onSubmit(): void {
    const { value: { Email, Password } } = this.form;
    const user: IUser = new User(Email, Password);

    this._registration.next(user);
  }
}
