import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthViewFlags } from '../../flags/auth.view.flags';
import { AuthViewService } from '../../services/auth.view.service';
import { ILength } from 'src/app/shared/interfaces/shared/ILength';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';
import { NSAUTH_LENGTH } from '../../namespaces/auth.namespaces';
import { LoginService } from '../../services/login.service';
import { OnInitMetadata } from 'src/app/shared/interfaces/hooks/OnInitMetadata';
import { AuthDataFlags } from '../../flags/auth.data.flags';
import { Observable } from 'rxjs';
import { IAction } from 'src/app/shared/interfaces/shared/IAction';
import { TAuthAction } from '../../types/auth.services.types';

@Component({
  selector: 'excel-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent implements OnInit, OnInitMetadata, OnDestroy {

  emitter$: Observable<IAction<TAuthAction>>;

  viewFlags: AuthViewFlags;
  dataFlags: AuthDataFlags;
  length: ISimple<ILength> = NSAUTH_LENGTH.FORM;

  constructor(
    private viewService: AuthViewService,
    public dataService: LoginService
  ) { }

  ngOnInit(): void {
    this.ngOnInitMetadata();
  }

  ngOnDestroy() {
    this.dataService.destroy(null, ['flags']);
    this.viewService.destroy(['flags', 'state']);
  }

  ngOnInitMetadata() {
    this.emitter$ = this.dataService.getEmitterStream();
    this.viewFlags = this.viewService.getFullFlags();
    this.dataFlags = this.dataService.getFullFlags();
  }

  show(flag: boolean): void {
     this.viewService.setFlag('isShowRegistration', flag);
  }
}
