import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectionStrategy } from '@angular/core';
import { ILength } from 'src/app/shared/interfaces/shared/ILength';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TLoginUser } from '../../types/auth.types';

@Component({
  selector: 'excel-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  
  @Input('length') length: ISimple<ILength>;
  @Input('request') request: boolean = false;
  @Output('show') _show = new EventEmitter<void>();
  @Output('login') _login = new EventEmitter<TLoginUser>();

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.init();
  }

  init(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      Email: ['', [
        Validators.required,
        Validators.email,
      ]],
      Password: ['', [
        Validators.required,
        Validators.minLength(this.length.Password.min),
        Validators.maxLength(this.length.Password.max),
      ]]
    })
  }

  isDisabledForm(): boolean {
    return this.form.invalid || this.request;
  }
  
  show(): void {
    this._show.emit();
  }

  onSubmit(): void {
    const { value: { Email, Password } } = this.form;
    const user: TLoginUser = { Email, Password }; 

    this._login.emit(user);
  }

}
