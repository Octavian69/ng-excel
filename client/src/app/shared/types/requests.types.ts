import { TKeyValue } from './shared.types';

export type TResponseAction = 'replace' | 'expand';
export type TSortOrientation = 'ascend' | 'descend';
export type TResetQuery = 'filters' | 'sort' | 'all';

export type TRequestKeysQuery<
    F extends object, 
    S extends object,
    FK extends keyof F & string = any,
    SK extends keyof S & string = any> 
= {
    filter?: TKeyValue<F[FK], FK>[],
    sort?: TKeyValue<S[SK], SK>[]
}

export type TSort<T> = {
    [key in keyof Partial<T>]: number;
};