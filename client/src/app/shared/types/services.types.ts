export type TDefaultStreamValue = {
    stream: string,
    value?: any
}