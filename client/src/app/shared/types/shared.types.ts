export type TKeyboardArrow = 'up' | 'down' | 'left' | 'right';
export type TVisibility = 'show' | 'hide';
export type TStatus = 'error' | 'warning' | 'success' | 'default';

export type TFile = 'IMG';
export type TFileErrorOption = {
    errorMessage: string;
    isValid: boolean;
}

export type TKeyValue<V = any, K extends string = string> = {
    key: K,
    value: V
}
export type TCommonObject<K extends string | number = string, V = any> = {
    [prop in K]: V
}
