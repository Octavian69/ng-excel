export interface IConfirmEvent<T extends string = string> {
    type: T;
    value: boolean;
    data: any;
}