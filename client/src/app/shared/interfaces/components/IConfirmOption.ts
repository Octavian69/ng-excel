export interface IConfirmOption<T extends string = string> {
    message: string;
    type: T;
    data?: any;
}