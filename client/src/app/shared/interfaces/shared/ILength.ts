import { ISimple } from './ISimple';

export interface ILength {
    min?: number,
    max?: number
}