export interface IAction<T extends string> {
    type: T;
    payload: any;
}