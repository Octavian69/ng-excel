import { IConfirmOption } from '../../components/IConfirmOption';

export class ConfirmOption<T extends string = string> implements IConfirmOption {
    constructor(
        public type: T,
        public message: string,
        public data: any = null
    ) {}
}