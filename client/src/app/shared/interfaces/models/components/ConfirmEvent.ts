import { IConfirmEvent } from '../../components/IConfirmEvent';

export class ConfirmEvent<T extends string = string> implements IConfirmEvent<T> {
    constructor(
        public type: T,
        public value: boolean,
        public data: any = null
    ) {}
}