import { Observable } from 'rxjs';
import { IAction } from '../shared/IAction';

export interface OnInitEmitterStream<T extends string = null> {
    ngOnInitEmiterStream(): void;
    emitter$?: Observable<IAction<T>>
}