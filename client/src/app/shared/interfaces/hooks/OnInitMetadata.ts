export interface OnInitMetadata {
    ngOnInitMetadata: () => void
}