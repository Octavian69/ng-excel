import { Observable, Subject } from 'rxjs';

export interface OnCanDeactivateComponent {
    ngOnCanDeactivate(): Observable<boolean> | Promise<boolean> | boolean;
    deactivate$?: Subject<boolean>;
}