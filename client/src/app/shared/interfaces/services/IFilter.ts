import { TSort } from '../../types/requests.types';

export interface IFilter<F extends object = any, S extends object = any> {
    filter: F;
    sort: TSort<S>;
}