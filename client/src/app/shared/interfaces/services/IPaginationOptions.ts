import { Page } from 'src/app/system-page/models/Page';
import { TResponseAction } from '../../types/requests.types';
import { IFilter } from './IFilter';

export interface IPaginationOptions<T extends IFilter> {
    page: Page;
    query?: T;
    action?: TResponseAction;
}