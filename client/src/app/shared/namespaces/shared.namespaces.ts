import { TFile, TCommonObject } from '../types/shared.types';

export namespace NSTOASTR {
    export const INFO: string = 'Информация.';
    export const SUCCESS: string = 'Успешно!';
    export const WARNING: string = 'Предупреждение!';
    export const ERROR: string = 'Внимание!';
}

export namespace NSFILE {
    export const TYPES: TCommonObject<TFile, string[]> = {
        IMG: ['image/jpeg', 'image/jpg', 'image/png']
    };

    export const SIZE: TCommonObject<TFile, number> = {
        IMG: 5
    }
}
