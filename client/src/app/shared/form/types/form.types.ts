import { TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';
import { ValidationErrors } from '@angular/forms';
import { TAntIconTheme, TAntTooltipPlacement } from '../../modules/ant/types/ant.types';


export type TMorpheme = 'suffix' | 'prefix';
export type TCustomTemplate = TemplateRef<any> | string;
export type TInputExtraMessage = TemplateRef<any> | 'length';
export type TInputType = 'text' | 'password' | 'number';
export type TInputStatus = 'error' | 'warning' | 'success' | 'validating';
export const InputActions = ['hide'] as const;
export type TInputAction = typeof InputActions[number];
export type TMorphemeTemplates = Array<TemplateRef<any> | TInputAction>;
export type TDebounceValidorParams<T, V> = [
    (value: T) => Observable<V>,
    (value: V) => ValidationErrors
]
export type TInputActionIcon = {
    icon: string,
    type: TInputAction,
    tooltip: string,
    tooltipPlacement: TAntTooltipPlacement,
    theme: TAntIconTheme
}