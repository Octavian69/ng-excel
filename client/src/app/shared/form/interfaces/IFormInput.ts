import { IFormControl } from './IFormControl';
import { TMorphemeTemplates, TInputExtraMessage, TInputType } from '../types/form.types';
import { TAntInputSize } from '../../modules/ant/types/ant.types';
import { ISimple } from '../../interfaces/shared/ISimple';

export interface IFormInput extends IFormControl {
    type: TInputType;
    size: TAntInputSize;
    feedback: boolean;
    extra: TInputExtraMessage;
    prefix: TMorphemeTemplates;
    suffix: TMorphemeTemplates;
    label: string;
    minlength: string | number;
    maxlength: string | number;
    placeholder: string;
    extraLengthCss: ISimple<string>;
}