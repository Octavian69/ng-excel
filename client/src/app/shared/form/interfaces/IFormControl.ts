import { NgControl } from '@angular/forms';

export interface IFormControl {
    ngControl: NgControl;
    value: any;
    disabled: boolean;
    fieldName: string;

    onChange: (v: string) => void;
    onTouched: () => void;
    init?: () => void;
    setValue: (value: any) => void
    getErrorMsg: (key: string, value: any) => string;
}