import { NgModule } from '@angular/core';
import { CoreModule } from '../../modules/core.module';
import { FormInputComponent } from '../components/form-input/form-input.component';
import { AntModule } from '../../modules/ant/modules/ant.module';

const declarations = [
    FormInputComponent
]

@NgModule({
    declarations,
    imports: [CoreModule, AntModule],
    exports: declarations
})
export class FormModule {}