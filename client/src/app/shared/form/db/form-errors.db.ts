export const DBFormErrors = {
    email: 'Поле "{{fieldName}}" должно быть формата email: "ivanov@mail.ru"',
    required: 'Поле "{{fieldName}}" обязательно для заполнения',
    minlength: 'Поле "{{fieldName}}" не может быть меньше {{requiredLength}} символов. Сейчас {{actualLength}}',
    maxlength: 'Поле "{{fieldName}}" не может быть больше {{requiredLength}} символов. Сейчас {{actualLength}}',
    existEmail: 'Пользователь с таким email уже существует'
}