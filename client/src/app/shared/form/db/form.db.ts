export const DBForm = {
    inputActions: [
        {
            icon: 'eye',
            type: 'hide',
            tooltip: 'Показать',
            tooltipPlacement: 'bottom',
            theme: 'twotone'
        },
        {
            icon: 'close-square',
            type: 'reset',
            tooltip: 'Сбросить значение',
            tooltipPlacement: 'bottom',
            theme: 'twotone'
        }
    ],
    extraLentghCss: {
        color: 'rgba(0,0,0,.45)'
    }
}