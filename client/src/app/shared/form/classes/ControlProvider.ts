import { forwardRef, Type, InjectionToken } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

export class ControlProvider {

    public provide: InjectionToken<ControlValueAccessor> = NG_VALUE_ACCESSOR;
    public multi: boolean = true;
    public useExisting: Type<any>

    constructor(Component) {
        this.useExisting = forwardRef(() => Component)
    }
}