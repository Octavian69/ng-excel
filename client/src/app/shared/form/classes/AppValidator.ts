import { AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable, BehaviorSubject } from 'rxjs';
import { debounceTime, map, switchMap, first } from 'rxjs/operators';
import { TDebounceValidorParams } from '../types/form.types';

export class AppValidator {

    static asyncDebounce<T, V>([request, cb]: TDebounceValidorParams<T, V>, ms: number = 300) {
        const sub$: BehaviorSubject<any> = new BehaviorSubject(null);
        const req$: Observable<ValidationErrors> = sub$.pipe(
            debounceTime(ms),
            switchMap(request),
            map(cb),
            first()
        ) 
        
        return (control: AbstractControl): Observable<ValidationErrors> => {
            sub$.next(control.value)

            return req$;
        }
    }

}