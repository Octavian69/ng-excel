import { Component, OnInit, Injector, Input, ChangeDetectionStrategy, ChangeDetectorRef, TemplateRef, AfterViewInit} from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { ControlProvider } from '../../classes/ControlProvider';
import { trim, getDataFromDB, titlecase, prefix } from 'src/app/shared/handlers/shared.handlers';
import { TInputType, TInputExtraMessage, TInputAction, TMorpheme, TMorphemeTemplates, TInputActionIcon } from '../../types/form.types';
import { errorHandler } from '../../handlers/form.handlers';
import { Bind } from 'src/app/shared/handlers/shared.decorators';
import { DBForm } from '../../db/form.db';
import { TAntInputSize } from 'src/app/shared/modules/ant/types/ant.types';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';
import { IFormInput } from '../../interfaces/IFormInput';

@Component({
  selector: 'excel-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
  providers: [new ControlProvider(FormInputComponent)],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormInputComponent implements OnInit, AfterViewInit, ControlValueAccessor, IFormInput {

  ngControl: NgControl;
  value: string = null;
  disabled: boolean = false;
  hide: boolean = true;
  inputActions: TInputActionIcon[] = getDataFromDB(DBForm, ['inputActions']);

  onChange: (v: string) => void = (v: string) => null;
  onTouched: () => void = () => null;

  @Input() type: TInputType = 'text';
  @Input() fieldName: string = '';
  @Input() size: TAntInputSize = 'default';
  @Input() feedback: boolean = true;
  @Input() extra: TInputExtraMessage = 'length';
  @Input() prefix: TMorphemeTemplates = [];
  @Input() suffix: TMorphemeTemplates = [];
  @Input() label: string = null;
  @Input() minlength: string | number = null;
  @Input() maxlength: string | number = null;
  @Input() placeholder: string = '';
  @Input() extraLengthCss: ISimple<string> = getDataFromDB(DBForm, ['extraLentghCss']);
  
  constructor(
    private injector: Injector,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.init();
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
  }

  init(): void {
    this.ngControl = this.injector.get(NgControl);
  }

  @Bind()
  getErrorMsg(key: string, value: any): string {
    return errorHandler(key, value, this.fieldName);
  }

  getTemplates(type: TMorpheme): TemplateRef<any>[] {
    return this[type]?.filter(t => typeof t !== 'string') as TemplateRef<any>[];
  }

  getVisibleIcon(): string {
    return this.hide ? 'eye-invisible' : 'eye';
  }
  
  getSize(): ISimple<boolean> {
    return {
      [this.size]: true
    }
  }

  isShowIcon(type: TMorpheme, action: TInputAction): boolean {
    return this[type].includes(action);
  }

  setHideStatus(): void {
    this.hide = !this.hide;
  }

  setValue(value: string): void {
    this.value = trim(value);
    this.onChange(this.value);
    this.onTouched();
    this.cdr.detectChanges();
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  writeValue(value: string): void {
    this.value = value;
    this.cdr.detectChanges();
  }

  setDisabledState(status: boolean) {
    this.disabled = status;
    this.cdr.detectChanges();
  }

  iconAction(type: TInputAction): void {
    const action: string = titlecase(type);
    const method: string = prefix(action, 'on');

    this[method]();
  }

  onHide(): void {
    const hideObject: TInputActionIcon = this.inputActions.find(o => o.type === 'hide');

    this.type = this.type === 'text' ? 'password' : 'text';
    hideObject.icon = this.type === 'text' ? 'eye-invisible' : 'eye';
    hideObject.tooltip = this.type === 'text' ? 'Скрыть' : 'Показать';
  }

  onReset(): void {
    this.setValue(null);
  }
}
