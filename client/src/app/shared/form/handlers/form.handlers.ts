import { DBFormErrors } from '../db/form-errors.db';

export function errorHandler(key: string, value: any, fieldName: string): string {
    try {
        const errorMsg: string = DBFormErrors[key];

        if(!errorMsg) throw Error('Отсутствует описание ошибки в form-errors.db.ts')
        const tags: string[] = errorMsg.match(/\{\{\w+\}\}/gi) || [];
        const errors = Object.assign({fieldName}, value);
    
        const completeMsg = tags.reduce((accum, current) => {
            const tag = current.replace(/[{}]/g, '');
    
            if(errors[tag]) accum = accum.replace(current, errors[tag]);
    
            return accum;
        }, errorMsg);
            
        return completeMsg;
    } catch(e) {
        console.error(e)
    }
}