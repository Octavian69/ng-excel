import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/compiler/src/core';

@NgModule({})
export class ServicesModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ServicesModule,
            providers: []
        }
    }
}