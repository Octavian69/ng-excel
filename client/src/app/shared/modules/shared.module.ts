import { NgModule } from '@angular/core';
import { AntModule } from './ant/modules/ant.module';
import { CoreModule } from './core.module';
import { ComponentsModule } from './components.module';
import { FormModule } from '../form/modules/form.module';
import { PipesModule } from './pipes.module';
import { DirectivesModule } from './directives.module';

const modules = [
    CoreModule,
    AntModule,
    ComponentsModule,
    PipesModule,
    DirectivesModule,
    FormModule
];

@NgModule({
    imports: modules,
    exports: modules,
})
export class SharedModule {}