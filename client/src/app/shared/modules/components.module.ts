import { NgModule } from '@angular/core';
import { CoreModule } from './core.module';
import { AntModule } from './ant/modules/ant.module';

import { MessageComponent } from '../components/message/message.component';
import { UploadComponent } from '../components/upload/upload.component';
import { PipesModule } from './pipes.module';
import { PreloaderComponent } from '../preloader/components/preloader/preloader.component';
import { ConfirmComponent } from '../confirm/components/confirm/confirm.component';


const declarations = [
    PreloaderComponent,
    MessageComponent,
    UploadComponent,
    ConfirmComponent
]

@NgModule({
    declarations,
    imports: [
        CoreModule,
        AntModule,
        PipesModule
    ],
    exports: declarations,
})
export class ComponentsModule {}