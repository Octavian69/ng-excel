import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const modules = [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
]

@NgModule({
    imports: [...modules, ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'})],
    exports: modules
})
export class CoreModule {}