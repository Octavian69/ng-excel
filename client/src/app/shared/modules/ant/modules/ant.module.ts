import { NgModule } from '@angular/core';
import { IconDefinition } from '@ant-design/icons-angular';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { 
    WhatsAppOutline, 
    QuestionOutline,
    LogoutOutline,
    SearchOutline,
    PlusOutline,
    AppstoreTwoTone, 
    EyeTwoTone,
    EyeInvisibleTwoTone,
    CloseSquareTwoTone,
    CloseCircleTwoTone,
    EditTwoTone,
    DeleteTwoTone,
    SaveTwoTone,
    ItalicOutline,
    BoldOutline,
    UnderlineOutline,
    AlignLeftOutline,
    AlignCenterOutline,
    AlignRightOutline,
    ExportOutline,
    FieldNumberOutline,
    PauseOutline,
    SyncOutline,
    DeleteOutline,
    SaveOutline,
    UploadOutline,
    DownloadOutline,
    PictureTwoTone,
    InfoCircleTwoTone,
    InfoCircleOutline,
    RollbackOutline
} from '@ant-design/icons-angular/icons';

const icons: IconDefinition[] = [
    WhatsAppOutline, 
    QuestionOutline,
    LogoutOutline,
    SearchOutline,
    PlusOutline,
    AppstoreTwoTone, 
    EyeTwoTone,
    EyeInvisibleTwoTone,
    CloseSquareTwoTone,
    CloseCircleTwoTone,
    EditTwoTone,
    DeleteTwoTone,
    SaveTwoTone,
    ItalicOutline,
    BoldOutline,
    UnderlineOutline,
    AlignLeftOutline,
    AlignCenterOutline,
    AlignRightOutline,
    ExportOutline,
    FieldNumberOutline,
    PauseOutline,
    SyncOutline,
    DeleteOutline,
    SaveOutline,
    UploadOutline,
    DownloadOutline,
    PictureTwoTone,
    InfoCircleTwoTone,
    InfoCircleOutline,
    RollbackOutline
];

const modules = [
    NzTableModule,
    NzGridModule,
    NzIconModule,
    NzToolTipModule,
    NzFormModule,
    NzInputModule,
    NzEmptyModule,
    NzButtonModule,
    NzUploadModule,
    NzDropDownModule
];

@NgModule({
    imports: [...modules, NzIconModule.forRoot(icons)],
    exports: modules
})
export class AntModule {}