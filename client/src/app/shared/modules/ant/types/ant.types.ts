export type TAntIconTheme = 'twotone' | 'outline' | 'fill';
export type TAntInputSize = 'default' | 'small' | 'large';
export type TAntBtn = 'primary' | 'dashed' | 'link' | 'default';
export type TAntTooltipPlacement = 'topLeft'
    | 'top'
    | 'topRight'
    | 'topLeft'
    | 'leftTop'
    | 'left'
    | 'leftBottom'
    |'rightTop'
    |'right'
    |'rightBottom'
    |'bottomLeft'
    |'bottom'
    |'bottomRight';

