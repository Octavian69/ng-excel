import { NgModule } from '@angular/core';
import { CoreModule } from './core.module';
import { DisabledBoxDirective } from '../directives/disabled-box.directive';

const declarations = [
    DisabledBoxDirective
];

@NgModule({
    declarations,
    imports: [CoreModule],
    exports: declarations
})
export class DirectivesModule{}