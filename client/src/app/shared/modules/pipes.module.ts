import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SanitizerPipe } from '../pipes/sanitizer.pipe';
import { HostPrefixPipe } from '../pipes/hostprefix.pipe';
import { BooleanPipe } from '../pipes/boolean.pipe';

const declarations = [
    SanitizerPipe,
    HostPrefixPipe,
    BooleanPipe
]

@NgModule({
    declarations,
    imports: [CommonModule],
    exports: declarations,
})
export class PipesModule {} 