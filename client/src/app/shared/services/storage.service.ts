import { Injectable } from '@angular/core';
import { ServicesModule } from '../modules/services.module';

export type TStorage = 'localStorage' | 'sessionStorage';

@Injectable({
    providedIn: ServicesModule
})
export class StorageService {
    getItem<T>(key: string, storage: TStorage = 'localStorage'): T {
        const value: string = window[storage].getItem(key);

        return JSON.parse(value);
    }

    removeItem(key: string, storage: TStorage = 'localStorage'): void {
        window[storage].removeItem(key);
    }

    removeItems(keys: string[], storage: TStorage = 'localStorage'): void {
        keys.forEach(k => this.removeItem(k, storage))
    }

    setItem<T>(key: string, value: T, storage: TStorage = 'localStorage'): void {
        const str: string = JSON.stringify(value);

        window[storage].setItem(key, str);
    }

    clear(storage: TStorage = 'localStorage') {
        window[storage].clear();
    }
}