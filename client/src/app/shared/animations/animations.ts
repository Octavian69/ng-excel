import { trigger, transition, style, animate, query, stagger, animateChild, AnimationTriggerMetadata, group } from '@angular/animations';
import { TResizeProperty } from 'src/app/system-page/excel-page/components/excel-table/types/table.types';

export const ANShowScale = (
    axis: 'X' | 'Y' = 'Y',
    ms: number = 300,
    from: string = '-100%',
    to: string = '0%'): AnimationTriggerMetadata => {

    const prop: string = `translate${axis}`;

    return trigger('ANShowScale', [
        transition(':enter', [
            style({
                position: 'relative',
                transform: `${prop}(${from})`,
                opacity: 0
            }),
            animate(ms, style({
                transform: `${prop}(${to})`,
                opacity: 1
            }))
        ]),
        transition(':leave', [
            animate(ms, style({
                position: 'relative',
                transform: `${prop}(${from})`,
                opacity: 0
            }))
        ]),
    ])
} 

export const ANStateScale = (
    axis: 'X' | 'Y' = 'Y',
    ms: number = 300,
    from: string = '-100%',
    to: string = '0%'
): AnimationTriggerMetadata => {
    
    const prop: string = `translate${axis}`;

    return trigger('ANStateScale', [
        transition('* => *', [
            style({
                position: 'relative',
                transform: `${prop}(${from})`,
                opacity: 0
            }),
            animate(ms, style({
                transform: `${prop}(${to})`,
                opacity: 1
            }))
        ])
    ])
} 

export const ANChildAnimate = (selectors: string = '@*'): AnimationTriggerMetadata => trigger('ANChildAnimate', [
    transition(':enter', [
        query(selectors, [
            animateChild()
        ], { optional: true })
    ]),
    transition(':leave', [
        query(selectors, [
            animateChild()
        ], { optional: true })
    ]),
])

export const ANQueryScale = (
        selector: string = '*', 
        step: number = 100, 
        time: number = 100
    ): AnimationTriggerMetadata => {
    return trigger('ANQueryScale', [
        transition(':enter', [
            query(selector, [
                style({
                    transform: 'scale(0)'
                }),
                stagger(step, [
                    animate(time, style({transform: 'scale(1)'}))
                ])
            ], { optional: true })
        ])
    ])
}

export const ANFade = (ms: string | number = 300) => trigger('ANFade', [
    transition(':enter', [
        style({ opacity: 0 }),
        animate(ms, style({ opacity: 1 }))
    ]),
    transition(':leave', [
        style({ opacity: 1 }),
        animate(ms, style({ opacity: 0 }))
    ]),
]);

export const ANFadeState = (ms: number | string = 300): AnimationTriggerMetadata => trigger('ANFadeState', [
    transition('* => *', [
        style({opacity: 0}),
        animate(ms, style({ opacity: 1 }))
    ])
]);

export const ANFadeChild = (
    ms: number = 300, 
    selectors: string = '@*'
): AnimationTriggerMetadata => trigger('ANFadeChild', [
    transition(':enter', [
        style({ opacity: 0 }),
        animate(ms, style({
            opacity: 1
        })),
        query(selectors, [
            animateChild()
        ], { optional: true })
    ]),
    transition(':leave', [
        query(selectors, [
            animateChild()
        ], { optional: true }),
        style({ opacity: 1 }),
        animate(ms, style({
            opacity: 0
        })),
    ])
])

export const ANSize = (
        prop: TResizeProperty = 'height', 
        ms: string | number = 300,
        from: string = '0px',
        to: string = '*'
    ): AnimationTriggerMetadata => trigger('ANSize', [
        transition(':enter', [
            style({ [prop]: from }),
            animate(ms, style({ [prop]: to }))
        ]),
        transition(':leave', [
            style({ [prop]: to }),
            animate(ms, style({ [prop]: from }))
        ]),
])

export const ANSizeState = (
        prop: TResizeProperty = 'height', 
        ms: string | number = 300,
        from: string = '0px',
        to: string = '*'
    ): AnimationTriggerMetadata => trigger('ANSizeState', [
        transition('* => *', [
            style({ [prop]: from }),
            animate(ms, style({ [prop]: to }))
        ])
])

