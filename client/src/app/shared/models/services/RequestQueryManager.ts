import { TResetQuery, TSort, TRequestKeysQuery } from "../../types/requests.types";
import { IFilter } from '../../interfaces/services/IFilter';
import { deepCopy } from '../../handlers/shared.handlers';

export class RequestQueryManager<F extends object = any, S extends object = any> {

    public previousFilter: F = null;
    public previousSort: TSort<S> = null;

    public defaultFilter: F = null;
    public defaultSort: TSort<S> = null

    public filter: F = null;
    public sort: TSort<S> = null;

    constructor(
        defaultQuery: IFilter<F, S>
    ) {
        this.initDefaultQuery(defaultQuery);
    }

    initDefaultQuery(defaultQuery: IFilter<F, S>): void {
        const { sort = null, filter = null } = defaultQuery;

        this.defaultFilter = filter;
        this.defaultSort = sort;

        this.previousFilter = deepCopy(filter);
        this.previousSort = deepCopy(sort);

        this.filter = deepCopy(filter);
        this.sort = deepCopy(sort);
    }

    get(isSetPrevious: boolean = true): IFilter<F, S> {
        const { filter, sort } = this;

        if(isSetPrevious) {
            this.previousFilter = Object.assign({}, filter);
            this.previousSort = Object.assign({}, sort);
        }

        return { filter, sort }
    }

    modifiedQuery(query: TRequestKeysQuery<F, S>): void {
        const { filter, sort } = query;

        if(filter && filter.length) {
            this.filter = filter.reduce((accum, { key, value }) => {
                if(value) accum[key] = value;
                else delete accum[key]

                return accum;
            }, Object.assign({}, this.defaultFilter))
        }

        if(sort && sort.length) {
            this.sort = sort.reduce((accum, { key, value }) => {
                if(value) {
                    accum[key] = value === 'ascend' ? 1 : -1;
                }

                return accum;
            }, Object.assign({}, this.defaultSort))
        } 
    }

    reset(type: TResetQuery) {
        switch(type) {
            case 'filters': {
                this.filter = this.defaultFilter;
                break;
            }

            case 'sort': {
                this.sort = this.defaultSort;
                break;
            }

            case "all": {
                this.filter = this.defaultFilter;
                this.sort = this.defaultSort;
                break;
            }
        }
    }

    isActiveFilterField(field: keyof F): boolean {
        return Boolean(this.previousFilter[field])
    }

    isDisabledSendFilter(field: keyof F): boolean {
        return this.filter[field] === this.previousFilter[field];
    }

    isDisabledSendSort(field: keyof S): boolean {
        return this.sort[field] === this.previousSort[field];
    }

    isDisabledResetFilter(field: keyof F): boolean {
        return !this.previousFilter[field];
    }

    resetFilterField<K extends keyof F>(field: K): void {
        const value: F[K] = this.defaultFilter[field] || null;

        this.filter[field] = value;
    }

    resetSortField<K extends keyof S>(field: K): void {
        const value: number = this.defaultSort[field] || null;

        if(value) this.sort[field] = value;
        else delete this.sort[field];
    }
}