import { IAction } from '../../interfaces/shared/IAction';

export class Action<T extends string, V> implements IAction<T> {
    constructor(
        public type: T,
        public payload: V
    ) {}
}