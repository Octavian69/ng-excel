import { IPaginationData } from '../../interfaces/services/IPaginationData';

export class PaginationData<T> implements IPaginationData<T> {
    constructor(
        public rows: T[] = [],
        public totalCount: number = 0
    ) {}
}