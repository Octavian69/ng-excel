import { IPaginationOptions } from '../../interfaces/services/IPaginationOptions';
import { Page } from 'src/app/system-page/models/Page';
import { TResponseAction } from '../../types/requests.types';
import { IFilter } from '../../interfaces/services/IFilter';

export class PaginationOptions<T extends IFilter> implements IPaginationOptions<T> {
    constructor(
        public page: Page,
        public query?: T,
        public action?: TResponseAction
    ) {}
}