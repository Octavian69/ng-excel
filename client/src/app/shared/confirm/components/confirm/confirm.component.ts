import { Component, OnInit, Input, ChangeDetectionStrategy, HostBinding } from '@angular/core';
import { ANShowScale, ANFadeChild } from 'src/app/shared/animations/animations';
import { ConfrimFlags } from '../../flags/Confirm.flags';
import { OnInitMetadata } from 'src/app/shared/interfaces/hooks/OnInitMetadata';
import { IConfirmOption } from 'src/app/shared/interfaces/components/IConfirmOption';
import { ConfirmService } from '../../services/confirm.service';

@Component({
  selector: 'excel-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANFadeChild(), ANShowScale('Y', 400, '100%')]
})
export class ConfirmComponent implements OnInit, OnInitMetadata {

  flags: ConfrimFlags;

  @HostBinding('@ANFadeChild') ANFadeChild;
  @Input() option: IConfirmOption;

  constructor(
    private confirmService: ConfirmService
  ) { }

  ngOnInit(): void {
    this.ngOnInitMetadata();
  }

  ngOnInitMetadata() {
    this.flags = this.confirmService.getFullFlags();
  }

  event(flag: boolean): void {
    this.confirmService.event(flag);
  }
}
