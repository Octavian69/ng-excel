import { Injectable } from '@angular/core';
import { ServicesModule } from '../../modules/services.module';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { IConfirmEvent } from '../../interfaces/components/IConfirmEvent';
import { ConfirmEvent } from '../../interfaces/models/components/ConfirmEvent';
import { IConfirmOption } from '../../interfaces/components/IConfirmOption';
import { ConfirmOption } from '../../interfaces/models/components/ConfirmOptions';
import { ConfrimFlags } from '../flags/Confirm.flags';
import { DataServiceManager } from '../../managers/models/DataServiceManager';

@Injectable({
    providedIn: ServicesModule
})
export class ConfirmService<T extends string = string> extends DataServiceManager<ConfrimFlags> {

    confirmOption$: BehaviorSubject<IConfirmOption> = new BehaviorSubject(null);
    confirm$: Subject<IConfirmEvent> = new Subject;

    constructor() {
        super(new ConfrimFlags);
    }

    option<V>(type: T, message?: string, data?: V): void {
        if(type === 'deactivate') {
            message = 'покинуть страницу не сохранив изменения'
        }
        const option: IConfirmOption<T> = new ConfirmOption(type, message, data);

        this.next('confirmOption$', option);
    }

    event(value: boolean): void {
        const { type, data } = this.getStreamValue('confirmOption$');
        const event: IConfirmEvent = new ConfirmEvent(type, value, data);

        this.next('confirm$', event);
    }

    confirmStream(): Observable<IConfirmEvent<T>> {
        return this.getStream<IConfirmEvent<T>>('confirm$');
    }

    optionStream(): Observable<IConfirmOption<T>> {
        return this.getStream<IConfirmOption<T>>('confirmOption$');
    }

    loading(flag: boolean = true) {
        this.setFlag('isLoading', flag);

        if(!flag) {
            this.next('confirmOption$', null);
        }
    }

    destroy(): void {
        super.destroy(['confirmOption$']);
    }
}