import { getDataFromDB } from '../../handlers/shared.handlers';

export class DatabaseManager<T extends object> {
    constructor(
        private db: T = null
    ) {}

    get<V>(keys: string[],  isCopy: boolean = true): V {
        return getDataFromDB(this.db, keys, isCopy);
    }
}