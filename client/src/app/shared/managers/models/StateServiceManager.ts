import { TServiceDestroy } from '../types/managers.types';
import { StateManager } from './StateManager';
import { DatabaseManager } from './DatabaseManager';

export class StateServiceManager
<
    F extends object = null, 
    S extends object = null, 
    DB extends object = null
> {
    private flags: StateManager<F>;
    private state: StateManager<S>;
    private db: DatabaseManager<DB>;

    constructor(
        flags: F = null,
        state: S = null,
        db: DB = null
    ) { this.initialize(flags, state, db) }

    private initialize(flags: F, state: S, db: DB): void {
        const options: any[] = [
            [flags, 'flags', StateManager],
            [state, 'state', StateManager],
            [db, 'db', DatabaseManager],
        ];

        options.forEach(( [state, key, Constructor] ) => {
            if(state) this[key] = new Constructor(state)
        })
    }

    getDataFromDB<T>(keys: string[], isCopy: boolean = true): T {
       return this.db.get(keys, isCopy)
    }

    getFullFlags(): F {
        return this.flags.getFull();
    }

    getFlag<K extends keyof F>(key: K): F[K] {
        return this.flags.get(key);
    }

    setFlag<K extends keyof F>(key: K, value: F[K]): void {
        this.flags.set(key, value);
    }

    destroyFlags(): void {
        this.flags.reset();
    }

    getFullState(): S {
        return this.state.getFull();
    }

    getState<K extends keyof S>(key: K): S[K] {
        return this.state.get(key);
    }

    setState<K extends keyof S>(key: K, value: S[K]): void {
        this.state.set(key, value);
    }

    destroyState(): void {
        this.state.reset();
    }

    destroy(states?: TServiceDestroy[]): void {
        if(states) {
            states.forEach(s => this[s]?.reset());
        }
    }
}