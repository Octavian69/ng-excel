import { Observable, Subject } from 'rxjs';

export class UnsubscribeManager {
    private unsubscriber$: Subject<boolean> = new Subject();

    unsubscribe(): void {
        this.unsubscriber$.next(true);
    }

    untilDestoyed(): Observable<boolean> {
        return this.unsubscriber$.asObservable();
    }
}