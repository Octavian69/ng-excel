import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { IAction } from '../../interfaces/shared/IAction';
import { TDefaultStreamValue } from '../../types/services.types';
import { Action } from '../../models/services/Action';
import { StateServiceManager } from './StateServiceManager';
import { TServiceDestroy } from '../types/managers.types';
import { UnsubscribeManager } from './UnsubscribeManager';

export class DataServiceManager<
        F extends object = null, 
        A extends string = null, 
        DB extends object = null,
        S extends object = null,
    > extends StateServiceManager<F, S, DB> {

    private emitter$: Subject<IAction<A>> = new Subject();
    private unsubscriber: UnsubscribeManager = new UnsubscribeManager();

    constructor(
        flags: F = null, 
        db: DB = null,
        state: S = null
    ) {
        super(flags, state, db);
    }

    getEmitterStream(): Observable<IAction<A>> {
        return this.emitter$.asObservable();
    }

    emit<V>(action: A, value?: V): void {
        this.emitter$.next(new Action<A, V>(action, value));
    }

    getStream<T>(streamName: string): Observable<T> {
        return this[streamName].asObservable();
    }

    getStreamValue<T>(streamName: string): T {
        return (this[streamName] as BehaviorSubject<T>).getValue();
    }

    next<T>(stremName: string, value: T): void {
        this[stremName].next(value);
    }

    destroyStreams(streams: Array<TDefaultStreamValue | string>): void {
        streams.forEach((item: TDefaultStreamValue | string) =>  {
            if(typeof item === 'string') {
                this[item].next(null);
            } else {
                const { stream, value } = item as TDefaultStreamValue;
                this[stream].next(value);
            }
        });
    }

    untilDestoyed(): Observable<boolean> {
        return this.unsubscriber.untilDestoyed();
    }

    unsubscribe(): void {
        this.unsubscriber.unsubscribe();
    }

    destroy(streams?: Array<TDefaultStreamValue | string>, states?: TServiceDestroy[]): void {
        if(streams) this.destroyStreams(streams)
        if(states)  super.destroy(states);

        this.unsubscribe();
    }
}