import { deepCopy } from '../../handlers/shared.handlers';

export class StateManager<T extends object> {

    private _defaultState: T;

    constructor(
        private state: T
    ) { this.init(state) }

    private init(state: T): void {
        this._defaultState = deepCopy(state);
    }

    getFull(): T {
        return this.state;
    }

    get<K extends keyof T>(key: K): T[K] {
        return this.state[key];
    }

    set<K extends keyof T>(key: K, value: T[K]): void {
        this.state[key] = value;
    }

    reset(): void {
        this.state = deepCopy(this._defaultState);
    }
}