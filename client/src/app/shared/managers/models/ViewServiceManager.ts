import { Observable } from 'rxjs';
import { StateServiceManager } from './StateServiceManager';
import { TServiceDestroy } from '../types/managers.types';
import { UnsubscribeManager } from './UnsubscribeManager';

export class ViewServiceManager
    <
        F extends object = null, 
        S extends object = null, 
        DB extends object = null
    > 
extends StateServiceManager<F, S, DB> {

    private unsubscriber: UnsubscribeManager = new UnsubscribeManager();
    
    constructor(
        flags: F = null,
        state: S = null,
        db: DB = null
    ) {
        super(flags, state, db);
    }

    untilDestroyed(): Observable<boolean> {
       return this.unsubscriber.untilDestoyed();
    }

    unsubscribe() {
        this.unsubscriber.unsubscribe();
    }

    destroy(states?: TServiceDestroy[]): void {
        if(states) super.destroy(states)

        this.unsubscribe();
    }
}



