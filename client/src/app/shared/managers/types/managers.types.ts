export type TServiceState = 'flags' | 'state' | 'db';
export type TServiceDestroy = Exclude<TServiceState, 'db'>;