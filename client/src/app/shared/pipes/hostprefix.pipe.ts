import { PipeTransform, Pipe } from '@angular/core';
import { prefix } from '../handlers/shared.handlers';
import { environment } from '@env/environment';

@Pipe({
    name: 'hostprefix'
})
export class HostPrefixPipe implements PipeTransform {

    transform(value: string) {
        return prefix(`/${value}`, environment.API_URL)
    }
}