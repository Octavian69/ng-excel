import { Pipe, PipeTransform } from '@angular/core';
import { TSanitizer } from './types/pipes.types';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
    name: 'sanitizer'
})
export class SanitizerPipe implements PipeTransform {

    constructor(
        private sanitizer: DomSanitizer
    ) {}

    transform(value: any, type: TSanitizer = 'url') {
        switch(type) {
            case 'url': {
                return this.sanitizer.bypassSecurityTrustUrl(value);
            }
            case 'html': {
                return this.sanitizer.bypassSecurityTrustHtml(value);
            }
            case 'style': {
                return this.sanitizer.bypassSecurityTrustStyle(value);
            }
            case 'script': {
                return this.sanitizer.bypassSecurityTrustScript(value);
            }
        }
    }
}