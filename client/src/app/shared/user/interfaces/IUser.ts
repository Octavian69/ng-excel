export interface IUser {
    Email: string;
    Password: string;
    Created: Date;
    Avatar: string;
    _id?: string;
}