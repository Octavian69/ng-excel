import { IUser } from '../interfaces/IUser';

export class User implements IUser{
    constructor(
        public Email: string,
        public Password: string,
        public Created: Date = new Date,
        public Avatar: string = null
    ) {}
}