import { Injectable } from '@angular/core';
import { ServicesModule } from '../../modules/services.module';
import { UserDataFlags } from '../flags/user.data.flags';
import { BehaviorSubject } from 'rxjs';
import { IUser } from '../interfaces/IUser';
import { StorageService } from '../../services/storage.service';
import { UserService } from './user.service';
import { JWTService } from '../../../auth-page/services/jwt.service';
import { takeUntil } from 'rxjs/operators';
import { PreloaderService } from '../../preloader/services/preloader.service';
import { IMessage } from '../../interfaces/services/IMessage';
import { ToastrService } from 'ngx-toastr';
import { NSTOASTR } from '../../namespaces/shared.namespaces';
import { TUserAction } from '../types/user.types';
import { DataServiceManager } from '../../managers/models/DataServiceManager';

@Injectable({
    providedIn: ServicesModule
})
export class UserDataService extends DataServiceManager<UserDataFlags, TUserAction> {

    user$: BehaviorSubject<IUser> = new BehaviorSubject(null);

    constructor(
        private toastr: ToastrService,
        private storage: StorageService,
        private preloaderService: PreloaderService,
        private userService: UserService,
        private jwt: JWTService
    ) {
        super(new UserDataFlags)
    }

    fetch(): void {
        this.preloaderService.preload(true);

        const user: IUser = this.getStreamValue('user$') || this.storage.getItem('user');

        if(user) {
            this.next('user$', user);
            this.preloaderService.preload(false);
        } else {
            const id: string = this.jwt.id();
            const next = (user: IUser) => {
                this.preloaderService.preload(false);
                this.next('user$', user);
                this.storage.setItem('user', user);
            }

            this.userService.fetch(id).pipe(
                takeUntil(this.untilDestoyed())
            )
            .subscribe({ next });
        }
    }

    uploadAvatar(file: File): void {
        this.setFlag('isUploadAvatar', true);

        const formData: FormData = new FormData();

        formData.append('avatar', file, file.name);
        
        const next = (user: IUser) => {
            this.next('user$', user);
            this.storage.setItem('user', user);
            this.setFlag('isUploadAvatar', false);
            this.emit('[USER] upload-avatar');
        }

        this.userService.uploadAvatar(formData).pipe(
            takeUntil(this.untilDestoyed())
        )
        .subscribe({ next });
    }

    remove(): void {
        const id = this.jwt.id();
        const next = ({ message }: IMessage) => {
            this.toastr.success(message, NSTOASTR.SUCCESS);
            this.emit('[USER] remove');
        }

        this.userService.remove(id).subscribe({ next })
    }

    destroy(): void {
        super.destroy(['user$']);
    }
}