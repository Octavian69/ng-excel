import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServicesModule } from '../../modules/services.module';
import { IUser } from '../interfaces/IUser';
import { Observable } from 'rxjs';
import { IMessage } from '../../interfaces/services/IMessage';

@Injectable({
    providedIn: ServicesModule
})
export class UserService {
    constructor(
        private http: HttpClient
    ) {}

    fetch(userId: string): Observable<IUser> {
        return this.http.get<IUser>(`@/user/get-by-id/${userId}`);
    }

    uploadAvatar(fd: FormData): Observable<IUser> {
        return this.http.put<IUser>('@/user/upload-avatar', fd);
    }

    remove(userId: string): Observable<IMessage> {
        return this.http.delete<IMessage>(`@/user/remove/${userId}`);
    }
}