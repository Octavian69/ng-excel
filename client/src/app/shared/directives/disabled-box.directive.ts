import { AfterViewInit, Directive, ElementRef, Input, OnDestroy, Renderer2 } from '@angular/core';

@Directive({
  selector: '[excelDisabledBox]'
})
export class DisabledBoxDirective implements AfterViewInit, OnDestroy {

  disabledEl$: HTMLElement;
  disabled: boolean;
  unsub$: Function;

  @Input('excelDisabledBox') set _disabled(value: boolean) {
    if(this.disabled !== value) {
      this.disabled = value;
      this.setDisabled();
    }
  };

  constructor(
    private el$: ElementRef, 
    private renderer: Renderer2
  ) {}

  ngAfterViewInit(): void {
    this.initialize();
  }

  ngOnDestroy() {
    if(this.unsub$) this.unsub$();
  }

  initialize(): void {
    const { nativeElement } = this.el$;

    this.renderer.setStyle(nativeElement, 'position', 'relative');
    this.disabledEl$ = this.renderer.createElement('div');
    this.renderer.addClass(this.disabledEl$, 'disable-box');
    this.renderer.appendChild(nativeElement, this.disabledEl$);
    this.unsub$ = this.renderer.listen(this.disabledEl$, 'click', this.click);

    this.setDisabled();
  }

  setDisabled(): void {
    if(this.disabledEl$) {
      const value: string = this.disabled ? 'block' : 'none';

      this.renderer.setStyle(this.disabledEl$, 'display', value);
    }
  }

  click = (e: MouseEvent): void => {
    if(this.disabled) e.stopPropagation();
  }
}
