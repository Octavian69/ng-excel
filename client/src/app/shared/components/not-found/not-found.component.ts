import { Component, ChangeDetectionStrategy } from '@angular/core';
import { LoginService } from 'src/app/auth-page/services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'excel-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotFoundComponent {

  constructor(
    private router: Router,
    private loginService: LoginService
  ) { }

  navigate(): void {
    const homePath: string = this.loginService.isLogged()
     ? '/dashboard' : '/login';
     
     this.router.navigateByUrl(homePath)
  }

}
