import { Component, Output, EventEmitter, ChangeDetectionStrategy, Input } from '@angular/core';
import { ANSizeState, ANSize, ANFade, ANShowScale, ANChildAnimate} from '../../animations/animations';
import { ToastrService } from 'ngx-toastr';
import { checkedFile } from '../../handlers/shared.handlers';
import { TFile } from '../../types/shared.types';
import { NSTOASTR } from '../../namespaces/shared.namespaces';

@Component({
  selector: 'excel-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANChildAnimate(), ANFade(), ANShowScale(), ANSize(), ANSizeState()]
})
export class UploadComponent {
  
  file: File;
  preview: string = null;
  
  @Input() type: TFile = 'IMG';
  @Input() request: boolean = false;
  @Output('upload') _upload = new EventEmitter<File>();
  @Output('hide') _hide = new EventEmitter();

  constructor(
    private toastr: ToastrService
  ) {}

  change(e: Event): void {
    const file: File = (e.target as HTMLInputElement).files[0];

    if(file) {
      const { errorMessage, isValid } = checkedFile(file, this.type);

      if(isValid) {
        this.file = file;
        this.preview = URL.createObjectURL(file);
      } else {
        this.toastr.warning(errorMessage, NSTOASTR.WARNING);
      }
    }
  }

  revoke(): void {
    URL.revokeObjectURL(this.preview);
  }

  hide(): void {
    this._hide.emit();
  }

  upload(): void {
    this._upload.emit(this.file);
  }
}
