import { Component, Input } from '@angular/core';
import { ISimple } from '../../interfaces/shared/ISimple';
import { getDataFromDB } from '../../handlers/shared.handlers';
import { ComponentsDB } from '../db/components.db';
import { TStatus } from '../../types/shared.types';

@Component({
  selector: 'excel-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent {

  @Input() message: string;
  @Input() loading: boolean = false;
  @Input() type: TStatus = 'default';
  @Input() loadingCss: ISimple<string> = getDataFromDB(ComponentsDB, ['message', 'loadingCss']);

  getMessageCss(): ISimple<boolean> {
    return {
      [this.type]: true
    }
  }
}
