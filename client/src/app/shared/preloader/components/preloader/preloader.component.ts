import { Component, ChangeDetectionStrategy, HostBinding } from '@angular/core';
import { ANFade } from 'src/app/shared/animations/animations';

@Component({
  selector: 'excel-preloader',
  templateUrl: './preloader.component.html',
  styleUrls: ['./preloader.component.scss'],
  animations: [ANFade()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreloaderComponent {
  @HostBinding('@ANFade') ANFade;
}
