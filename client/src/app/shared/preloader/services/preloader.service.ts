import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, delay } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class PreloaderService {
    preload$: BehaviorSubject<boolean> = new BehaviorSubject(false);

    get(): Observable<boolean> {
        return this.preload$.asObservable().pipe(delay(0));
    }

    preload(flag: boolean): void {
        this.preload$.next(flag);
    }
}