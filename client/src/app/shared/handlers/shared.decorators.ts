type TDecoratorHandler = (target: any, fnName: string, desriptor: PropertyDescriptor) => PropertyDescriptor;

export function Bind():  TDecoratorHandler {
    return (target: any, fnName: string, descriptor: PropertyDescriptor): PropertyDescriptor => {
        const { value } = descriptor;

        return {
            get() {
                return value.bind(this);
            }
        }
    }
}