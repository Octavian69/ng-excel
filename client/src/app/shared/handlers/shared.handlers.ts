import { TKeyValue, TFile, TFileErrorOption } from '../types/shared.types';
import { NSFILE } from '../namespaces/shared.namespaces';

export function trim(value): string {
    return value ? value.trim().replace(/\s+/g, ' ') : value;
}

export function prefix(str: string, tag: string): string {
    return `${tag}${str}`;
}

export function titlecase(value: string): string {
    return value[0].toUpperCase() + value.slice(1);
}

export function suffix(str: string, tag: string): string {
    return `${str}${tag}`;
}

export function deepCompare<T, O>(v1: T, v2: O): boolean {
    return JSON.stringify(v1) === JSON.stringify(v2);
}

export function deepCopy<T>(value: T): T {
    return JSON.parse(JSON.stringify(value));
}

export function getDataFromDB<T extends object>(db: T, keys: string[], isCopy: boolean = true) {
    let value: any = db;
    keys = keys.concat();

    while(keys.length) {
        value = value[keys[0]];
        keys.shift();

        if(!value) break;
    }

    return isCopy ? deepCopy(value) : value;
};

export function debounce(ms: number, cb: Function): (value: any) => void {
    let timeout;

    return (value: any): void => {

        if(timeout) clearTimeout(timeout);

        timeout = setTimeout(_ => {
            cb(value);
        }, ms)
    }
}

export function attribute(attr: string, value?: any): string {
    attr = value ? attr + `="${value}"` : attr;

    return `[${attr}]`;
}


export function listener(el$: HTMLElement, events: TKeyValue<Function>[], action: 'remove' | 'add'): void {
    const method = `${action}EventListener`;

    events.forEach(({ key: event, value: cb }) => el$[method](event, cb));
}

export function checkedFile(file: File, fileType: TFile): TFileErrorOption {
    const maxSize: number = NSFILE.SIZE[fileType];
    const successTypes: string[] = NSFILE.TYPES[fileType];

    const { size, type } = file;

    let errorMessage: string;

    const isValidType: boolean = successTypes.includes(type);
    const isValidSize: boolean = size / Math.pow(1024, 2)  <= maxSize;

    switch(true) {
        case !isValidType: {
            errorMessage = `
                Недопустимый формат файла.\n
                Выберите файл формата: ${successTypes.join(',\n')}
            `
            break;
        }
        case !isValidSize: {
            errorMessage = `Максимальный размер файла ${maxSize} Mb.`
            break;
        }
    }

    const isValid: boolean = errorMessage ? false : true;

    return { errorMessage, isValid };
}
