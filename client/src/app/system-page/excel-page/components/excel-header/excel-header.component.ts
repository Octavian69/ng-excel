import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NSEXCEL_LENGTH } from '../../namespaces/excel.namespaces';
import { trim } from 'src/app/shared/handlers/shared.handlers';
import { ANShowScale } from 'src/app/shared/animations/animations';

@Component({
  selector: 'excel-excel-header',
  templateUrl: './excel-header.component.html',
  styleUrls: ['./excel-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANShowScale()]
})
export class ExcelHeaderComponent implements OnInit {

  titleControl: FormControl;
  maxlength: number = NSEXCEL_LENGTH.TABLE.Title.max;
  isShowTitleCtrl: boolean;

  @Input() title: string;
  @Input() disableSave: boolean = false;
  @Input() isCanRemove: boolean = false;
  @Input('isShowTitleCtrl') set _isShowTitleCtrl(value: boolean) {
    this.titleControl?.setValue(this.title);
    this.isShowTitleCtrl = value;
  }

  @Output('showTitleCtrl') _showTitleCtrl = new EventEmitter<boolean>(); 
  @Output('setTitle') _setTitle = new EventEmitter<string>();
  @Output('back') _back = new EventEmitter<void>();
  @Output('remove') _remove = new EventEmitter<void>();
  @Output('save') _save = new EventEmitter<void>();

  ngOnInit(): void {
    this.init();
  }

  init(): void {
    this.initFormControl();
  }

  initFormControl(): void {
    this.titleControl = new FormControl(this.title);
  }

  setTitle(): void {
    const value: string = trim(this.titleControl.value);
    const isEqual: boolean = Object.is(value, this.title);

    if(isEqual || !value) {
      this.showTitleCtrl(false);
    } else {
      this._setTitle.emit(value);
    }
  }

  showTitleCtrl(flag: boolean): void {
    this._showTitleCtrl.emit(flag);
  }

  back(): void {
    this._back.emit();
  }

  remove(): void {
    this._remove.emit();
  }

  save(): void {
    this._save.emit();
  }
}
