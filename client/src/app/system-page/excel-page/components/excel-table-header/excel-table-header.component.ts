import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';

@Component({
  selector: 'excel-excel-table-header',
  templateUrl: './excel-table-header.component.html',
  styleUrls: ['./excel-table-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExcelTableHeaderComponent {

  @Input() columns: string[] = [];
  @Input() colSize: ISimple<number>;

  getSize(idx: number): ISimple<string> {
    const size: number = this.colSize[idx]
    const width: string = size ? size  + 'px' : '';

    return { width }
  }
}
