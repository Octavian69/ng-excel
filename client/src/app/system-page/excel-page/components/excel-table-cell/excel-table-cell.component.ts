import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { TTableResizer } from '../excel-table/types/table.types';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';

@Component({
  selector: 'excel-excel-table-cell',
  templateUrl: './excel-table-cell.component.html',
  styleUrls: ['./excel-table-cell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExcelTableCellComponent {

  @Input() id: string;
  @Input() resizer: TTableResizer
  @Input() content: string; 
  @Input() styles: ISimple<string>;
  @Input() width: ISimple<string>;
  @Input() selected: boolean = false;

  getCss(): ISimple<boolean> {
    const { selected } = this;
    const styles: ISimple<boolean> = Object.values(this.styles || []).reduce((accum, cls) => {
      accum[cls] = true;
      return accum;
    }, {});

    return { 
      selected,
      ...styles
    }
  }
}
