import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { ExcelViewService } from '../../services/excel.view.service';
import { ExcelDataService } from '../../services/excel.data.service';
import { takeUntil } from 'rxjs/operators';
import { UnsubscribeManager } from 'src/app/shared/managers/models/UnsubscribeManager';

@Component({
  selector: 'excel-excel-formula',
  templateUrl: './excel-formula.component.html',
  styleUrls: ['./excel-formula.component.scss']
})
export class ExcelFormulaComponent extends UnsubscribeManager implements OnInit, OnDestroy {

  value: string;
  formulaControl: FormControl;

  formula$: Observable<string> = this.dataService.getStream('formula$');

  constructor(
    private dataService: ExcelDataService,
    private viewService: ExcelViewService
  ) { super() }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy() {
    this.unsubscribe();
  }

  init(): void {
    this.initFormulaControl();
    this.initFormulaSub();
  }

  initFormulaControl(): void {
    this.formulaControl = new FormControl();
  }

  initFormulaSub(): void {
    this.formula$.pipe(
      takeUntil(this.untilDestoyed())
    )
    .subscribe(value => this.value = value);
  }

  action(): void {
    this.viewService.formula(this.value);
  }
}
