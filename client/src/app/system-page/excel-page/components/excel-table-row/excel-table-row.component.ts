import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { ITable } from 'src/app/system-page/interfaces/ITable';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';
import { splitCellId } from '../excel-table/handlers/table.handlers';

@Component({
  selector: 'excel-excel-table-row',
  templateUrl: './excel-table-row.component.html',
  styleUrls: ['./excel-table-row.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExcelTableRowComponent {


  @Input() row: string[] = [];
  @Input() selectedCells: string[] = [];
  @Input() rowIndex: number;
  @Input() table: ITable;
  @Input() rowSize: ISimple<number>;
  @Input() colSize: ISimple<number>;

  getRowSize(): ISimple<string> {
    const height: string = this.rowSize ? this.rowSize + 'px' : '';

    return { height }
  }

  getCellSize(id: string) {
    const [ row, col ] = splitCellId(id);
    const size: number = this.colSize[col];
    const width: string = size ? size + 'px' : '';
    
    return { width }
  }

  getContent(id: string): string {
    return this.table.CellContent[id] || '';
  }

  isSelected(id: string): boolean {
    return this.selectedCells.includes(id);
  }

  getStyles(id: string) {
    return this.table.CellStyles[id];
  }
}
