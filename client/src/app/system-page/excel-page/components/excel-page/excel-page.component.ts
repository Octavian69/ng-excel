import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, HostListener } from '@angular/core';
import { ExcelViewService } from '../../services/excel.view.service';
import { ExcelViewFlags } from '../../flags/excel.view.flags';
import { Observable, Subject } from 'rxjs';
import { ITable } from 'src/app/system-page/interfaces/ITable';
import { TToolbarIcon } from '../excel-toolbar/types/toolbar.types';
import { Router, ActivatedRoute } from '@angular/router';
import { TTableSize, TTableEvent } from '../excel-table/types/table.types';
import { ExcelState } from '../../states/excel.state';
import { ExcelDataService } from '../../services/excel.data.service';
import { ExcelDataFlags } from '../../flags/excel.data.flags';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';
import { OnInitMetadata } from 'src/app/shared/interfaces/hooks/OnInitMetadata';
import { OnCanDeactivateComponent } from 'src/app/shared/interfaces/hooks/OnCanDeactivateComponent';
import { filter, map, first, tap, takeUntil } from 'rxjs/operators';
import { TExcelConfirmAction, TExcelAction } from '../../types/excel.services.types';
import { OnInitEmitterStream } from 'src/app/shared/interfaces/hooks/OnInitEmitterStream';
import { ConfirmService } from 'src/app/shared/confirm/services/confirm.service';
import { OnInitConfirmStream } from 'src/app/shared/interfaces/hooks/OnInitConfirmStream';
import { IConfirmEvent } from 'src/app/shared/interfaces/components/IConfirmEvent';
import { IAction } from 'src/app/shared/interfaces/shared/IAction';
import { UnsubscribeManager } from 'src/app/shared/managers/models/UnsubscribeManager';

@Component({
  selector: 'excel-excel-page',
  templateUrl: './excel-page.component.html',
  styleUrls: ['./excel-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExcelPageComponent extends UnsubscribeManager implements 
  OnInit, 
  OnDestroy,
  OnInitMetadata, 
  OnCanDeactivateComponent, 
  OnInitEmitterStream,
  OnInitConfirmStream
{
  //Observables
  table$: Observable<ITable> = this.dataService.getStream('table$');
  deactivate$: Subject<boolean> = new Subject();

  //metadata
  dataFlags: ExcelDataFlags
  viewFlags: ExcelViewFlags;
  state: ExcelState;

  //components properties
  toolbarIcons: TToolbarIcon[] = this.viewService.getDataFromDB(['toolbar', 'icons']);
  toolbarDefaultStyles: ISimple<string> = this.viewService.getDataFromDB(['toolbar', 'defaultStyles']);
  tableSize: TTableSize = this.viewService.getDataFromDB(['table', 'defaultSize']);
  tableEvents: TTableEvent[] = this.viewService.getDataFromDB(['table', 'tableEvents']);

  @HostListener('window:beforeunload', ['$event'])
    closeWindow(e: any) {
      const isUnsavedTable: boolean = this.dataService.getFlag('isUnsavedTable');

      return isUnsavedTable ? false : true;
    }

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    public viewService: ExcelViewService,
    public dataService: ExcelDataService,
    public confirmService: ConfirmService<TExcelConfirmAction>
  ) { super() }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {
    this.dataService.destroy();
    this.viewService.destroy(['flags', 'state']);
    this.unsubscribe()
  }

  ngOnInitMetadata(): void {
    this.dataFlags = this.dataService.getFullFlags();
    this.viewFlags = this.viewService.getFullFlags();
    this.state = this.viewService.getFullState();

    this.ngOnInitConfirmStream();
    this.ngOnInitEmiterStream();
  }

  ngOnInitEmiterStream(): void {
    const emitterTypes: TExcelAction = this.viewService.getDataFromDB(['excel', 'emitterTypes']);

    this.dataService.getEmitterStream().pipe(
      filter(({ type }) => emitterTypes.includes(type)),
      takeUntil(this.untilDestoyed()),
    )
    .subscribe(( { type, payload }: IAction<TExcelAction> ) => {
      switch(type) {
        case '[EXCEL] create-table': {
          this.router.navigate(['/excel', (payload as ITable)._id])
          break;
        }
        case '[EXCEL] remove-table': {
          this.confirmService.destroy();
          this.router.navigateByUrl('/dashboard');
          break;
        }
      }
    })
  }

  ngOnInitConfirmStream(): void {
    this.confirmService.confirmStream().pipe(
      filter(({ type }: IConfirmEvent) => type === '[EXCEL] remove-table'),
      takeUntil(this.untilDestoyed()),
    ).subscribe(( { value }: IConfirmEvent ) => {
      if(value) {
        this.confirmService.loading();
        this.dataService.remove();
      } else {
        this.confirmService.loading(false);
      }
    })
  }

  ngOnCanDeactivate(): Observable<boolean> | boolean {
    if(this.dataService.getFlag('isUnsavedTable')) {
      this.confirmService.option('deactivate');

      return this.confirmService.confirmStream()
        .pipe(
          filter(({ type }) => type === 'deactivate'),
          first(),
          map(({ value }) => value),
          tap(_ => this.confirmService.destroy())
        )
    }

    return true;
  }

  init(): void {
    this.fetch();
    this.ngOnInitMetadata();
  }

  fetch() {
    const id: string = this.activeRoute.snapshot.paramMap.get('id');
    this.dataService.fetch(id);
  }

  getToolbarStyles(): ISimple<string> {
    const currentCell: string = this.viewService.getState('currentCell');
    const table: ITable = this.dataService.getStreamValue('table$');

    return table?.CellStyles[currentCell];
  }

  showTitleCtrl(flag: boolean): void {
    this.viewService.setFlag('isShowTitleCtrl', flag);
  }

  back() {
    this.viewService.destroy();
    this.router.navigateByUrl('/dashboard');
  }

  save(): void {
    const table: ITable = this.dataService.getStreamValue('table$');

    if(!table._id) {
      this.dataService.create();
    } else {
      this.dataService.updateTable();
    }
  }

  confirmRemove(): void {
    this.confirmService.option('[EXCEL] remove-table', 'удалить таблицу');
  }
}
