export type TToolbarIcon = {
    icon: string,
    type: string,
    className: string,
    multi: boolean,
    active: boolean
    default: boolean,
}