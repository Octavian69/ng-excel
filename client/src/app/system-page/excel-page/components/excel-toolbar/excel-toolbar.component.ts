import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter, HostBinding } from '@angular/core';
import { TToolbarIcon } from './types/toolbar.types';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';
import { ANQueryScale } from 'src/app/shared/animations/animations';

@Component({
  selector: 'excel-excel-toolbar',
  templateUrl: './excel-toolbar.component.html',
  styleUrls: ['./excel-toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANQueryScale('.btn')]
})
export class ExcelToolbarComponent {

  currentStyles: ISimple<string>;

  @HostBinding('@ANQueryScale') showScale;
  
  @Input() icons: TToolbarIcon[] = [];
  @Input() defaultStyles: ISimple<string>;
  @Input('currentStyles') set _currentStyles(value: ISimple<string>) {
    if(value) {
      this.currentStyles = Object.assign({}, this.defaultStyles, value);
    } else {
      this.currentStyles = { ...this.defaultStyles };
    }
   
  };
  @Output('setStyles') _setStyles = new EventEmitter<TToolbarIcon>();

  isActive(opt: TToolbarIcon): boolean {
    const { type, className } = opt;

    return  this.currentStyles[type] === className;
  }

  isDefaultStyles(styles: string[]): boolean {
    const defaultLength = Object.keys(this.defaultStyles).length;
    const stylesLength = Object.keys(styles).length;

    if(defaultLength !== stylesLength) return false;

    const every: boolean = Object.keys(styles).every(key => {
      return styles[key] === this.defaultStyles[key];
    })

    return every;
  }

  action(option: TToolbarIcon): void {

    const { multi, className, type } = option;

    if(multi && this.currentStyles[type] === className) return;

    this._setStyles.emit(option);
  }

}
