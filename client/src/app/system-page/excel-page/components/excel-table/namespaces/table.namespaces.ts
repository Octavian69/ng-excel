export namespace NSTABLE {
    export const CELL_BORDER_WIDTH: number = 1;
    export const RESIZER_SIZE: number = 5;
    export const MIN_CELL_WIDTH: number = 60;
    export const MIN_CELL_HEIGHT: number = 35;
    export const CELL_HEIGHT = 35;
    export const CELL_WIDTH = 95;
}