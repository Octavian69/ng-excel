export type TTableRow = 'header' | 'row';
export type TTableResizer = 'column' | 'row';
export type TResizeProperty = 'height' | 'width';
export type TTableSize = {
    cols: {
        start: number,
        end: number
    },
    rows: number
};
export type TTableEvent = {
    attr: string,
    event: string
};

