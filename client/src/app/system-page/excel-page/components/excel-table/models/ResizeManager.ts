import { TResizeProperty, TTableResizer } from '../types/table.types';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';
import { NSTABLE } from '../namespaces/table.namespaces';

export class ResizeManager {
    parent$: HTMLElement;
    parentCss: ISimple<number>;
    startPosition: number;
    tablePosition: number;
    startSize: number;
    resizeProperty: TResizeProperty;
    delta: number = 0;

    constructor(
        public table$: HTMLElement,
        public resizer$: HTMLElement,
        public resizeType: TTableResizer,
        public value: number = 0,
        public cells$: HTMLElement[] = [],
    ) {
        this.init();
    }

    init(): void {
        this.initMetadata();
        this.initResizer();
    }

    initMetadata(): void {
        this.parent$ = this.resizer$.closest('[data-resizer-parent]');
        this.resizeProperty = this.isColumn() ? 'width' : 'height'
        this.startSize = this.parent$.getBoundingClientRect()[this.resizeProperty];
        this.tablePosition = +this.parent$.getAttribute('data-resizer-parent');
    }

    isColumn(): boolean {
        return Object.is(this.resizeType, 'column');
    }

    initResizer(): void {
        const { top, left } = this.resizer$.getBoundingClientRect();
        const { clientHeight: height, clientWidth: width } = this.table$;
        const sizeVal: number = this.isColumn() ? height - NSTABLE.CELL_BORDER_WIDTH : width;

        this.startPosition = this.isColumn() ? left : top;
        this.setResizeStyles('add', sizeVal);
    }

    setResizeStyles(action: 'add' | 'remove', size?: number): void {
        this.resizer$.classList[action]('active');
        const property: TResizeProperty = this.isColumn() ? 'height' : 'width';

        if(action === 'add') {
            this.resizer$.style[property] = size + 'px';
        } else {
            this.resizer$.style[property] = '';
        }
    }

    getChilds(): HTMLElement[] {
        const matcher: '^' | '$' = this.isColumn() ? '$' : '^';
        const matcherPosition: string = this.isColumn() ? `:${this.tablePosition}` : `${this.tablePosition}:`
        const attr: string = `[data-cell-id${matcher}="${matcherPosition}"]`;
        const childs: ArrayLike<HTMLElement> = this.table$.querySelectorAll(attr);

        return Array.from(childs);
    }

    setChilds(): void {
        if(this.isColumn() && this.isChanChange()) {
            const childs: HTMLElement[] = this.getChilds();
            childs.forEach(c$ => this.setSize(c$, this.value));
        }
    }

    end(): void {
        this.setChilds();
        this.destroy();
    }

    destroy(): void {
        this.setResizeStyles('remove');
    }

    setSize(el$: HTMLElement, value: number): void {
        el$.style[this.resizeProperty] = value + 'px';
    }

    isChanChange(): boolean {
        return Math.abs(this.delta) > NSTABLE.RESIZER_SIZE;
    }

    set(e: Event): void {
        const eventProp = this.isColumn() ? 'clientX' : 'clientY';
        this.delta = e[eventProp] - this.startPosition;

        if(this.isChanChange()) {
            this.value = this.startSize + this.delta;
            this.setSize(this.parent$, this.value);
        }
    }
}