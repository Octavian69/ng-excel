import { Component, OnInit, Input, ChangeDetectionStrategy, ViewChild, ElementRef, Inject, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { ITable } from 'src/app/system-page/interfaces/ITable';
import { TTableSize, TTableEvent, TTableResizer } from './types/table.types';
import { ExcelViewService } from '../../services/excel.view.service';
import { ResizeManager } from './models/ResizeManager';
import { DOCUMENT } from '@angular/common';
import { listener, trim } from 'src/app/shared/handlers/shared.handlers';
import { TKeyValue, TKeyboardArrow } from 'src/app/shared/types/shared.types';
import { ExcelState } from '../../states/excel.state';
import { splitCellId, joinCellId } from './handlers/table.handlers';
import { NSTABLE } from './namespaces/table.namespaces';
import { timer } from 'rxjs';

@Component({
  selector: 'excel-excel-table',
  templateUrl: './excel-table.component.html',
  styleUrls: ['./excel-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExcelTableComponent implements OnInit, AfterViewInit {

  columns: string[] = [];
  rows: number[] = [];
  resizeManager: ResizeManager;
  maxHeight: string = '';

  @Input() table: ITable;
  @Input() size: TTableSize;
  @Input() state: ExcelState;
  @Input() tableEvents: TTableEvent[] = [];
  

  @ViewChild('tableRef', { static: true, read: ElementRef }) tableRef: ElementRef;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private viewService: ExcelViewService,
    private cdr: ChangeDetectorRef
  ) {}
  
  ngOnInit(): void {
    this.init();
  }

  ngAfterViewInit() {
    this.initTableHeight();
  }

  init(): void {
    this.initColumns();
    this.initRows();
    this.viewService.select('0:0'); 
  }

  initTableHeight(): void {
    timer(0).subscribe(() => {
      const { nativeElement } = this.tableRef;
      const { clientHeight } = this.document.documentElement;
  
      const { top } = (nativeElement as HTMLDivElement).getBoundingClientRect();
  
      this.maxHeight = clientHeight - top + 'px';
  
      this.cdr.detectChanges();
    })
  }

  initColumns(): void {
    const { cols: {start, end} } = this.size;
    const length: number = end - start;

    this.columns = Array.from({length}, (_, idx: number) => {
      return String.fromCharCode(start + idx)
    })
  }

  initRows(): void {
    const { rows: length } = this.size;
    this.rows = Array.from({length}, (_, idx) => idx);
  }

  getRow(rowIdx: number): string[] {
    return this.columns.map((col, i) => `${rowIdx}:${i}`);
  }

  destroyResize(): void {
    this.resizeManager.destroy();
    this.documentListener('remove');
    this.resizeManager = null;
  }

  documentListener(action: 'add' | 'remove'): void {
    const { documentElement } = this.document;
    const events: TKeyValue<Function>[] = [
        {key: 'mousemove', value: this.onMouseMove},
        {key: 'mouseup', value: this.onMouseUp},
      ];

    listener(documentElement, events, action);
  }

  onMouseUp = (): void => {
    const { value, resizeType, tablePosition } = this.resizeManager;
    const { MIN_CELL_WIDTH, MIN_CELL_HEIGHT } = NSTABLE;
    const min: number = resizeType === 'column' ? MIN_CELL_WIDTH : MIN_CELL_HEIGHT;
    const size: number = value < min ? min : value;
    this.resizeManager.end();
    this.destroyResize();

    this.viewService.setSize(resizeType, { [tablePosition]: size });
  }

  onMouseDown(e: MouseEvent): void {
    const el$ = e.target as HTMLDivElement;
    const resizerType = el$.getAttribute('data-resizer-type') as TTableResizer;

    if(resizerType) {
      this.onResize(el$, resizerType);
    }
  }

  onMouseMove = (e: MouseEvent) => {
    this.resizeManager.set(e);
  }

  onResize(resizer$: HTMLDivElement, resizeType: TTableResizer) {
    const { nativeElement: table$ } = this.tableRef;
  
    this.resizeManager = new ResizeManager(
      table$,
      resizer$, 
      resizeType);

    this.documentListener('add');
  }

  id(e: Event): string {
    const cell$ = e.target as HTMLElement;
    const id: string =  cell$.getAttribute('data-cell-id');

    return id;
  }

  getCellById(id: string): HTMLElement {
    return this.tableRef.nativeElement.querySelector(`[data-cell-id="${id}"]`);
  }

  select(e: MouseEvent): void {
    const id: string = this.id(e);

    if(id) {
      if(e.shiftKey) {
        this.viewService.selectGroup(id);
      } else {
        this.viewService.select(id)
      }
    }
  }

  inputCell(e: Event): void {
    const { textContent } = e.target as HTMLDivElement;
    
    this.viewService.inputCell(trim(textContent));
  }

  arrow(side: TKeyboardArrow): void {
    const currentCellId: string = this.viewService.getState('currentCell');
    const currentCell$: HTMLElement = this.getCellById(currentCellId);
    const [row, col] = splitCellId(currentCellId);

    let id: string;

    switch(side) {
      case 'up': {
        id = joinCellId(row - 1, col);
        break;
      }
      case 'down': {
        id = joinCellId(row + 1, col);
        break;
      }
      case 'left': {
        id = joinCellId(row, col - 1);
        break;
      }
      case 'right': {
        id = joinCellId(row, col + 1);
        break;
      }
    }

    const nextCell$: HTMLElement = this.getCellById(id);
    
    if(nextCell$) {
      currentCell$.blur();
      nextCell$.focus();
      this.viewService.select(id);
    } 
  }

  selectNext(e: Event) {
    e.preventDefault();
    this.arrow('down');
  }
}
