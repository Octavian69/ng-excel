export function splitCellId(id: string, separator: string = ':'): number[] {
    return id.split(separator).map(Number);
}

export function joinCellId(row: number, col: number, separator: string = ':'): string {
    return `${row}${separator}${col}`;
}

export function getRange(start: number, end: number): number[] {
    if(start > end) [start, end] = [end, start];
    const length: number = end - start + 1;

    return Array.from({ length }, (_, i) => start + i);
}

export function selectedCellsGroup(currentId: string, targetId: string): string[] {
    const [currRow, currCol] = splitCellId(currentId);
    const [targetRow, targetCol] = splitCellId(targetId);

    const rows: number[] = getRange(currRow, targetRow);
    const cols: number[] = getRange(currCol, targetCol);

    const ids: string[] = rows.reduce((accum, row) => {
        const complete: string[] = cols.map(col => joinCellId(row, col));
        return accum.concat(complete);
    },[])

    return ids;
}