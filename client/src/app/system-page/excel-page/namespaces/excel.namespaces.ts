import { ILength } from 'src/app/shared/interfaces/shared/ILength';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';

export namespace NSEXCEL {
    
}

export namespace NSEXCEL_LENGTH {
    export const TABLE: ISimple<ILength> = {
        Title: {
            max: 70
        }
    }
}