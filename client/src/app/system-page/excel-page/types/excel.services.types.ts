import { TDeactivate } from 'src/app/shared/confirm/types/confirm.types';

export type TExcelAction = 
  '[EXCEL] create-table' 
| '[EXCEL] change-table' 
| '[EXCEL] remove-table' 
| '[EXCEL] save-table' 
| '[EXCEL] formula-change' 
| TDeactivate;
export type TExcelConfirmAction =  '[EXCEL] remove-table' | TDeactivate;