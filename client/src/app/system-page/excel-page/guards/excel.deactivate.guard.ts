import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { OnCanDeactivateComponent } from 'src/app/shared/interfaces/hooks/OnCanDeactivateComponent';
import { Observable } from 'rxjs';

@Injectable()
export class ExcelDeactivateGuard implements CanDeactivate<OnCanDeactivateComponent> {

    canDeactivate(component: OnCanDeactivateComponent): 
        Observable<boolean> | Promise<boolean> | boolean
    {
        return component.ngOnCanDeactivate();
    }
}