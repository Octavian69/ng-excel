export class ExcelDataFlags {
    public isSaveReq: boolean = false;
    public isRemoveTable: boolean = false;
    public isUnsavedTable: boolean = false;
}