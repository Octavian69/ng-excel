import { Injectable } from '@angular/core';
import { ExcelService } from './excel.service';
import { ITable } from '../../interfaces/ITable';
import { takeUntil } from 'rxjs/operators';
import { Table } from '../../models/Table';
import { StorageService } from 'src/app/shared/services/storage.service';
import { ExcelDataFlags } from '../flags/excel.data.flags';
import { BehaviorSubject } from 'rxjs';
import { PreloaderService } from 'src/app/shared/preloader/services/preloader.service';
import { IMessage } from 'src/app/shared/interfaces/services/IMessage';
import { TExcelAction } from '../types/excel.services.types';
import { ToastrService } from 'ngx-toastr';
import { NSTOASTR } from 'src/app/shared/namespaces/shared.namespaces';
import { DataServiceManager } from 'src/app/shared/managers/models/DataServiceManager';

@Injectable()
export class ExcelDataService extends DataServiceManager<ExcelDataFlags, TExcelAction> {

    table$: BehaviorSubject<ITable> = new BehaviorSubject(null);
    formula$: BehaviorSubject<string> = new BehaviorSubject('');

    constructor(
        private toastr: ToastrService,
        private storage: StorageService,
        private preloaderService: PreloaderService,
        private excelService: ExcelService,
    ) {
        super(new ExcelDataFlags);
    }

    fetch(id: string): void {
        const tableFromStorage: ITable = this.storage.getItem('table');
        const isUnsavedTable: boolean = this.storage.getItem('isUnsavedTable') || false;

        this.setFlag('isUnsavedTable', isUnsavedTable);
        this.storage.setItem('isUnsavedTable', isUnsavedTable);
        this.preloaderService.preload(true);

        if(id === '0' || tableFromStorage) {
            const table: ITable = tableFromStorage || new Table;

            this.preloaderService.preload(false);

            this.next('table$', table);
        } else {
             const next = (table: ITable) => {
                this.preloaderService.preload(false);
                this.next('table$', table);
            }

            this.excelService.fetch(id).pipe(
                takeUntil(this.untilDestoyed())
            ).subscribe({ next })
        }
    }

    create(): void {
        this.setFlag('isSaveReq', true);
        this.preloaderService.preload(true);

        const candidate: ITable = this.getStreamValue('table$');
        const next = (table: ITable) => {
            this.update<ITable>('[EXCEL] save-table', table);
            this.emit('[EXCEL] create-table', table);
            this.toastr.success(`Таблица "${table.Title}" создана.`, NSTOASTR.SUCCESS);
            this.preloaderService.preload(false);
        }

        this.excelService.create(candidate).pipe(
            takeUntil(this.untilDestoyed())
        ).subscribe({ next })
    }

    updateTable(): void {
        this.setFlag('isSaveReq', true);
        this.preloaderService.preload(true);

        const candidate: ITable = this.getStreamValue('table$');

        const next = (table: ITable) => {
            this.update<ITable>('[EXCEL] save-table', table);
            this.toastr.success(`Таблица "${table.Title}" отредактирована.`, NSTOASTR.SUCCESS);
            this.preloaderService.preload(false);
        }

        this.excelService.update(candidate).pipe(
            takeUntil(this.untilDestoyed())
        ).subscribe({ next })
    }

    remove(): void {
        this.setFlag('isRemoveTable', true);

        const { _id } = this.getStreamValue<ITable>('table$');

        const next = ({ message }: IMessage) => {
            this.toastr.success(message, NSTOASTR.SUCCESS);
            this.setFlag('isRemoveTable', false);
            this.storage.removeItem('isUnsavedTable');
            this.emit('[EXCEL] remove-table', message);
        }

        this.excelService.remove(_id).pipe(
            takeUntil(this.untilDestoyed())
        )
        .subscribe({ next })
    }

    update<T>(action: TExcelAction, value: T): void {
          switch(action) {
            case '[EXCEL] change-table': {
                const table: ITable = Object.assign(new Table, value);
                this.next('table$', table);
                this.storage.setItem('table', value);
                this.storage.setItem('isUnsavedTable', true);
                this.setFlag('isUnsavedTable', true);
                break;
            }
            case '[EXCEL] save-table': {
                this.next('table$', value);
                this.setFlag('isUnsavedTable', false);
                this.setFlag('isSaveReq', false);
                this.storage.setItem('isUnsavedTable', false);
                this.storage.setItem('table', value);
                break;
            }
            case '[EXCEL] formula-change': {
                this.next('formula$', value);
                break;
            }
        }
    }

    destroy(): void {
        super.destroy(['table$', 'formula$'], ['flags']);
        this.storage.removeItems(['table', 'isUnsavedTable']);
        this.preloaderService.preload(false);
    }
}