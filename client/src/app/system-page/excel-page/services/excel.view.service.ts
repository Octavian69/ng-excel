import { Injectable } from '@angular/core';
import { ExcelViewFlags } from '../flags/excel.view.flags';
import { ITable } from '../../interfaces/ITable';
import { ExcelDB, TExcelDB } from '../db/excel.db';
import { ExcelState } from '../states/excel.state';
import { selectedCellsGroup } from '../components/excel-table/handlers/table.handlers';
import { TToolbarIcon } from '../components/excel-toolbar/types/toolbar.types';
import { deepCopy, deepCompare } from 'src/app/shared/handlers/shared.handlers';
import { TTableResizer } from '../components/excel-table/types/table.types';
import { ExcelDataService } from './excel.data.service';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';
import { ViewServiceManager } from 'src/app/shared/managers/models/ViewServiceManager';

@Injectable()
export class ExcelViewService extends ViewServiceManager<ExcelViewFlags, ExcelState, TExcelDB> {
    constructor(
        private dataService: ExcelDataService
    ) {
        super(new ExcelViewFlags, new ExcelState, ExcelDB);
    }

    setTitle(Title: string): void {
        const table: ITable = this.dataService.getStreamValue('table$');
        const update: ITable = Object.assign({}, table, { Title });

        this.setFlag('isShowTitleCtrl', false);
        this.update(update);
    }

    setSize(type: TTableResizer, value: ISimple<number>): void {
        const table: ITable = this.dataService.getStreamValue('table$');
        const sizeState: keyof Pick<ITable, 'RowSize' | 'ColSize'> = 
            type == 'column'
                ? 'ColSize'
                : 'RowSize';
        const state: ISimple<number> = Object.assign({}, table[sizeState], value);

        table[sizeState] = state;
        this.update(table);
    }

    select(id: string): void {
        const { CellContent } = this.dataService.getStreamValue<ITable>('table$');
        const textContent: string = CellContent[id];

        this.setState('currentCell', id);
        this.setState('selectedCells',[id]);

        this.dataService.update('[EXCEL] formula-change', textContent);
    }

    selectGroup(id: string): void {
        const currentCell: string = this.getState('currentCell');
        const selectedCells: string[] = selectedCellsGroup(currentCell, id);

        this.setState('selectedCells', selectedCells);
    }

    formula(value): void {
         const table: ITable = this.dataService.getStreamValue('table$');
         const selectedCells: string[] = this.getState('selectedCells');
         const CellContent: ISimple<string> = selectedCells.reduce((accum, id) => {
             if(!value) delete accum[id]
             else accum[id] = value;
            
            return accum;
         }, { ...table.CellContent })

         const update: ITable = Object.assign({}, table, { CellContent });

         this.update(update);
    }

    inputCell(textContent: string): void {
        const table: ITable = this.dataService.getStreamValue('table$');
        const currentCell: string = this.getState('currentCell');
        
        if(!textContent) {
            delete table.CellContent[currentCell]
        } else {
            const CellContent = { ...table.CellContent, [currentCell]: textContent }
            table.CellContent = CellContent;
        }

        this.dataService.update('[EXCEL] formula-change',textContent.slice(0, 100));
        this.update(table);
    }

    setStyles(option: TToolbarIcon): void {
        const table: ITable = this.dataService.getStreamValue('table$');
        const selectedCells: string[] = this.getState('selectedCells');
        const defaultStyles: ISimple<string> = this.getDataFromDB(['toolbar', 'defaultStyles']);
        const { CellStyles } = table;
        const { type, className } = option;
        const value: ISimple<string> = { [type]: className };
        const styles: ISimple<ISimple<string>> = selectedCells.reduce((accum, id) => {
            const isExist: boolean = accum[id] && accum[id][type] === className;
  
            if(isExist) delete accum[id][type];
            else accum[id] = Object.assign({}, accum[id], value);

            return accum;
        }, {...CellStyles});
        

        Object.keys(styles).forEach(id => {
            const isDefault = deepCompare(styles[id], defaultStyles);

            if(isDefault) delete styles[id];
        });

        table.CellStyles = deepCopy(styles);
        this.update(table);
    }

    update(update: ITable): void {
        this.dataService.update<ITable>('[EXCEL] change-table', update);
    }
}