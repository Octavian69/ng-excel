import { Injectable } from '@angular/core';
import { ServicesModule } from 'src/app/shared/modules/services.module';
import { HttpClient } from '@angular/common/http';
import { ITable } from '../../interfaces/ITable';
import { Observable } from 'rxjs';
import { IMessage } from 'src/app/shared/interfaces/services/IMessage';

@Injectable({
    providedIn: ServicesModule
})
export class ExcelService {

    constructor(
        private http: HttpClient
    ) {}

    fetch(id: string): Observable<ITable> {
        return this.http.get<ITable>(`@/excel/get-by-id/${id}`);
    }

    create(table: ITable): Observable<ITable> {
        return this.http.post<ITable>(`@/excel/create`, table);
    }

    update(table: ITable): Observable<ITable> {
        return this.http.patch<ITable>(`@/excel/edit/${table._id}`, table);
    }

    remove(id: string): Observable<IMessage> {
        return this.http.delete<IMessage>(`@/excel/remove/${id}`);
    }
}