  export const ExcelDB = {
      excel: {
        emitterTypes: ['[EXCEL] create-table', '[EXCEL] remove-table']
      },
      toolbar: {
          icons: [
            {
                icon: 'align-left',
                className: 'align-left',
                type: 'textAlign',
                multi: true
            },
            {
                icon: 'align-center',
                className: 'align-center',
                type: 'textAlign',
                multi: true
            },
            {
                icon: 'align-right',
                className: 'align-right',
                type: 'textAlign',
                multi: true
            },
            {
                icon: 'bold',
                type: 'fontWeight',
                className: 'bold',
                multi: false
            },
            {
                icon: 'italic',
                type: 'fontStyle',
                className: 'italic',
                multi: false
            },
            {
                icon: 'underline',
                className: 'underline',
                type: 'textDecoration',
                multi: false
            }
        ],
        defaultStyles: { 'textAlign': 'align-left' }
      },
      table: {
        defaultSize: {
            cols: {
                start: 65,
                end: 91
            },
            rows: 20
        },
        tableEvents: [
            {attr: 'data-resizer-type', event: 'onResize'},
            {attr: 'data-cell-id', event: 'onCell'},
        ],

    }
  };


  export type TExcelDB = typeof ExcelDB;