import { NgModule } from '@angular/core';

import { ExcelViewService } from '../services/excel.view.service';

import { ExcelRoutingModule } from './excel-routing.module';
import { ExcelHeaderComponent } from '../components/excel-header/excel-header.component';
import { ExcelToolbarComponent } from '../components/excel-toolbar/excel-toolbar.component';
import { ExcelFormulaComponent } from '../components/excel-formula/excel-formula.component';
import { ExcelTableComponent } from '../components/excel-table/excel-table.component';
import { ExcelTableRowComponent } from '../components/excel-table-row/excel-table-row.component';
import { ExcelTableCellComponent } from '../components/excel-table-cell/excel-table-cell.component';
import { ExcelPageComponent } from '../components/excel-page/excel-page.component';
import { ExcelTableHeaderComponent } from '../components/excel-table-header/excel-table-header.component';
import { ExcelDataService } from '../services/excel.data.service';
import { ExcelDeactivateGuard } from '../guards/excel.deactivate.guard';
import { SharedModule } from 'src/app/shared/modules/shared.module';

@NgModule({
    declarations: [
        ExcelPageComponent,
        ExcelHeaderComponent,
        ExcelToolbarComponent,
        ExcelFormulaComponent,
        ExcelTableComponent,
        ExcelTableRowComponent,
        ExcelTableCellComponent,
        ExcelTableHeaderComponent,
    ],
    imports: [
        ExcelRoutingModule,
        SharedModule
    ],
    providers: [
        ExcelDeactivateGuard,
        ExcelViewService,
        ExcelDataService
    ]
})
export class ExcelModule {}