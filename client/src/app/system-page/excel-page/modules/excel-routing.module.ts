import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExcelPageComponent } from '../components/excel-page/excel-page.component';
import { ExcelDeactivateGuard } from '../guards/excel.deactivate.guard';

const routes: Routes = [
    { path: '', component: ExcelPageComponent, canDeactivate: [ExcelDeactivateGuard] }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExcelRoutingModule {}