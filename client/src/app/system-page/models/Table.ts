import { ITable } from '../interfaces/ITable';
import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';

export class Table implements ITable {
    public Title: string = 'Новая таблица';
    public Created: Date = new Date;
    public LastVisit: Date = new Date;
    public CellContent: ISimple<string> = {};
    public CellStyles: ISimple<ISimple<string>> = {};
    public ColSize: ISimple<number> = {};
    public RowSize: ISimple<number> = {};
    public User: string = null;
    public _id?: string;

    constructor() {}

    setLastVisit() {
        this.LastVisit = new Date();
    }
}