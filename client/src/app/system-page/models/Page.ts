export class Page {
    constructor(
        public limit: number = 20,
        public skip: number = 0
    ) {}
}