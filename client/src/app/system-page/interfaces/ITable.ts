import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';

export interface ITable {
    Title: string;
    Created: Date;
    LastVisit: Date;
    CellContent: ISimple<string>;
    CellStyles: ISimple<ISimple<string>>;
    ColSize: ISimple<number>;
    RowSize: ISimple<number>;
    User: string;
    _id?: string;
}