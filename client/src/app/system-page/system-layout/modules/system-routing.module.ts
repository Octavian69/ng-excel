import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SystemLayoutComponent } from '../components/system-layout/system-layout.component';
import { SystemGuard } from '../guards/system.guard';

const routes: Routes = [
    {path: '', component: SystemLayoutComponent, canActivate: [SystemGuard], children: [
        { path: 'excel/:id', 
            loadChildren: () => import('../../excel-page/modules/excel.module')
                .then(({ ExcelModule }) => ExcelModule),
        },
        { 
            path: 'dashboard',
            loadChildren: () => import('../../dashboard-page/modules/dashboard.module')
                .then(({ DashboardModule }) => DashboardModule)
        }
    ]}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SystemRoutingModule {}