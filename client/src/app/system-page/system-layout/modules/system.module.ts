import { NgModule } from '@angular/core';
import { CoreModule } from 'src/app/shared/modules/core.module';
import { SystemLayoutComponent } from '../components/system-layout/system-layout.component';
import { SystemRoutingModule } from './system-routing.module';
import { SystemGuard } from '../guards/system.guard';
import { ComponentsModule } from 'src/app/shared/modules/components.module';
import { ServicesModule } from 'src/app/shared/modules/services.module';

@NgModule({
    declarations: [
        SystemLayoutComponent
    ],
    imports: [
        CoreModule,
        ComponentsModule,
        SystemRoutingModule,
        ServicesModule
    ],
    providers: [
        SystemGuard
    ]
})
export class SystemModule {}