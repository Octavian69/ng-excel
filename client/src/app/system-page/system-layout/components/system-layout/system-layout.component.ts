import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserDataService } from 'src/app/shared/user/services/user.data.service';
import { ConfirmService } from 'src/app/shared/confirm/services/confirm.service';
import { PreloaderService } from 'src/app/shared/preloader/services/preloader.service';
import { Observable } from 'rxjs';
import { IConfirmOption } from 'src/app/shared/interfaces/components/IConfirmOption';

@Component({
  selector: 'excel-system-layout',
  templateUrl: './system-layout.component.html',
  styleUrls: ['./system-layout.component.scss'],
})
export class SystemLayoutComponent implements OnInit, OnDestroy {

  preload$: Observable<boolean> = this.preloaderService.get();
  confirmOption$: Observable<IConfirmOption> = this.confirmService.getStream('confirmOption$');

  constructor(
    private userDataService: UserDataService,
    public confirmService: ConfirmService,
    private preloaderService: PreloaderService
  ) { }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {
    this.userDataService.destroy();
  }

  init(): void {
    this.initUser();
  }

  initUser(): void {
    this.userDataService.fetch();
  }
}
