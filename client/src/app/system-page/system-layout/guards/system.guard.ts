import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginService } from 'src/app/auth-page/services/login.service';

@Injectable()
export class SystemGuard implements CanActivate, CanActivateChild {

    constructor(
        private router: Router,
        private loginService: LoginService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean> | Promise<boolean> | boolean
    {
        const isLogged: boolean = this.loginService.isLogged();

        if(!isLogged) {
            this.router.navigateByUrl('/login');

            return false;
        }

        return true;
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean> | Promise<boolean> | boolean
    {
        return this.canActivate(route, state)
    }
}