import { ISimple } from 'src/app/shared/interfaces/shared/ISimple';

export namespace NSDASHBOARD {

    export const LIMIT: ISimple<number> = {
        Table: 5
    }
}