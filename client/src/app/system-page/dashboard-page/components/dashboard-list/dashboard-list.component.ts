import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ITable } from 'src/app/system-page/interfaces/ITable';

@Component({
  selector: 'excel-dashboard-list',
  templateUrl: './dashboard-list.component.html',
  styleUrls: ['./dashboard-list.component.scss']
})
export class DashboardListComponent implements OnInit {

  @Input() tables: ITable[] = [];

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  navigate(id: string): void {
    this.router.navigate(['/excel', id]);
  }

}
