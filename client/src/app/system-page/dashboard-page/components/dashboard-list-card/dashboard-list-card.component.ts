import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { ITable } from 'src/app/system-page/interfaces/ITable';
import { TDashboardTable } from '../../types/dashboard.types';
import { Table } from 'src/app/system-page/models/Table';
import { suffix } from 'src/app/shared/handlers/shared.handlers';

@Component({
  selector: 'excel-dashboard-list-card',
  templateUrl: './dashboard-list-card.component.html',
  styleUrls: ['./dashboard-list-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardListCardComponent {

  @Input() table: ITable = new Table();
  @Input() type: TDashboardTable;

  @Output('navigate') _navigate = new EventEmitter<string>();

  navigate(): void {
    const id: string = this.type === 'create' ? '0' : this.table._id;
    
    this._navigate.emit(id);
  }

  getImgLogoUrl(): string {
    const img: string = this.type === 'create' 
    ? 'table-new.png'
    : 'table-purple.png'

    return suffix('/assets/img/dashboard/', img);
  }

}
