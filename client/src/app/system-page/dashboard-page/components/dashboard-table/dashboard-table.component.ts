import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Inject, ElementRef, Renderer2, ViewChild, AfterViewInit, OnDestroy, Input } from '@angular/core';
import { Page } from 'src/app/system-page/models/Page';
import { NSDASHBOARD } from '../../namespaces/dashboard.namespaces';
import { ITable } from 'src/app/system-page/interfaces/ITable';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { DOCUMENT } from '@angular/common';
import { Observable } from 'rxjs';
import { DashboardState } from '../../state/DashboardState';
import { DashboardViewService } from '../../services/dashboard.view.service';
import { DashboardDataService } from '../../services/dashboard.data.service';
import { DashboardDataFlags } from '../../flags/dashboard.data.flags';
import { IPaginationOptions } from 'src/app/shared/interfaces/services/IPaginationOptions';
import { PaginationOptions } from 'src/app/shared/models/services/PaginationOptions';
import { Router } from '@angular/router';
import { IPaginationData } from 'src/app/shared/interfaces/services/IPaginationData';
import { OnInitEmitterStream } from 'src/app/shared/interfaces/hooks/OnInitEmitterStream';
import { OnInitMetadata } from 'src/app/shared/interfaces/hooks/OnInitMetadata';
import { takeUntil, filter } from 'rxjs/operators';
import { ConfirmService } from 'src/app/shared/confirm/services/confirm.service';
import { OnInitConfirmStream } from 'src/app/shared/interfaces/hooks/OnInitConfirmStream';
import { TDashboardAction, TDashboardFilters, TDashboardSorted, TDashboardQuery } from '../../types/dashboard.types';
import { RequestQueryManager } from 'src/app/shared/models/services/RequestQueryManager';
import { DashboardViewFlags } from '../../flags/dashboard.view.flags';
import { trim } from 'src/app/shared/handlers/shared.handlers';
import { UnsubscribeManager } from 'src/app/shared/managers/models/UnsubscribeManager';

@Component({
  selector: 'excel-dashboard-table',
  templateUrl: './dashboard-table.component.html',
  styleUrls: ['./dashboard-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardTableComponent extends UnsubscribeManager implements 
  OnInit, 
  AfterViewInit, 
  OnInitEmitterStream,
  OnInitConfirmStream,
  OnInitMetadata,
  OnDestroy 
{
  tables$: Observable<IPaginationData<ITable>> = this.dataService.getStream('tables$');

  page: Page = new Page(NSDASHBOARD.LIMIT.Table);
  query: RequestQueryManager<TDashboardFilters, TDashboardSorted>;

  viewFlags: DashboardViewFlags;
  dataFlags: DashboardDataFlags;
  state: DashboardState;

  @ViewChild('mainRef') mainRef: ElementRef;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private cdr: ChangeDetectorRef,
    private renderer: Renderer2,
    private router: Router,
    private viewService: DashboardViewService,
    private dataService: DashboardDataService,
    private confirmService: ConfirmService<TDashboardAction>
  ) { super() }

  ngOnInit(): void {
    this.init();
  }

  ngAfterViewInit() {
    this.removePageSizePrefix();
  }

  ngOnInitMetadata(): void {
    this.ngOnInitEmiterStream();
    this.ngOnInitConfirmStream();

    this.viewFlags =  this.viewService.getFullFlags();
    this.dataFlags = this.dataService.getFullFlags();
    this.state = this.viewService.getFullState();

    const defaultQuery: TDashboardQuery = this.viewService.getDataFromDB(['table', 'defaultQuery'])
    this.query = new RequestQueryManager(defaultQuery)
  }

  ngOnInitConfirmStream(): void {
    this.confirmService.confirmStream().pipe(
      takeUntil(this.untilDestoyed()),
      filter(({ type }) => type === '[DASHBOARD] remove-tables'),
    ).subscribe(({ value, data: tablesIds }) => {
      if(value) {
        this.confirmService.loading();
        this.dataService.remove(tablesIds as string[]);
      } else {
        this.confirmService.loading(false);
      }
    })
  }

  ngOnInitEmiterStream(): void {
    this.dataService.getEmitterStream().pipe(
      takeUntil(this.untilDestoyed()),
      filter(({ type }) => type === '[DASHBOARD] remove-tables')
    ).subscribe(({ payload }) => {
      const { ids, rowsLength } = payload;
      const selectedTables: string[] = this.viewService.getState('selectedTables');
      const update: string[] = selectedTables.filter(t => !(ids as string[]).includes(t));
     
      this.confirmService.loading(false);
      this.viewService.setState('selectedTables', update);

      const isSendRequest: boolean = !rowsLength && this.page.skip > 0;

      if(isSendRequest) {
        this.page.skip--;
        this.fetch();
      }
    })
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  init(): void {
    this.ngOnInitMetadata();
    this.dataService.fetch();
  }

  fetch(): void {
    const event: IPaginationOptions<TDashboardQuery> = new PaginationOptions(this.page, this.query.get());
    this.dataService.next('fetch$', event);
  }

  removePageSizePrefix(): void {
    const { documentElement } = this.document;

    // interval(300).pipe(
    //   map(_ => documentElement.querySelector('[ng-reflect-label]')),
    //   skipWhile($el => !$el),
    //   take(1),
    //   untilDestroyed(this),
    // ).subscribe(($el: HTMLElement) => {
    //   this.renderer.setProperty(
    //       $el, 
    //       'textContent', 
    //       $el.textContent.slice(0, 2)
    //   )
    // })
  }

  trackByFn(idx: number, table: ITable): string {
    return table._id;
  }

  isDisabledSort(): boolean {
    const tables: IPaginationData<ITable> = this.dataService.getStreamValue('tables$');

    return tables?.totalCount < NSDASHBOARD.LIMIT.Table;
  }

  isActiveQuery(field: keyof TDashboardFilters): boolean {
    return this.query.isActiveFilterField(field);
  }

  isEmpty(): boolean {
    const tables:  IPaginationData<ITable> = this.dataService.getStreamValue('tables$');

    return !tables?.rows.length;
  }

  isDisabled(): boolean {
    const { isRemoveReq, isFetchReq } = this.dataFlags;

    return isRemoveReq || isFetchReq;
  }

  isDisabledSendFilter(field: keyof TDashboardFilters): boolean {
    return this.query.isDisabledSendFilter(field);
  }

  isSelected(id: string): boolean {
    return this.state?.selectedTables.includes(id);
  }

  isSelectedAll(): boolean {
    const { rows }: IPaginationData<ITable> = this.dataService.getStreamValue('tables$');
    const selectedTables = this.viewService.getState('selectedTables');

    return rows.length > 0 && rows.length === selectedTables.length;
  }

  select(id: string): void {
    this.viewService.select(id);
    this.cdr.detectChanges();
  }

  selectAll(value: boolean): void {
    const { rows } = this.dataService.getStreamValue('tables$')
    this.viewService.selectAll(value, rows);
  }

  changeFilter(value: string, key: keyof TDashboardFilters): void {
    const filter = [ { key, value: trim(value) } ];
    
    this.query.modifiedQuery({ filter });
  }

  isDisabledReset(field: keyof TDashboardFilters) {
    return this.query.isDisabledResetFilter(field);
  }

  resetField(field: keyof TDashboardFilters) {
    this.viewService.setFlag('isShowTitleSearch', false);
    this.query.resetFilterField(field);
    this.page.skip = 0;
    this.fetch();
  }

  search(): void {
    this.viewService.setFlag('isShowTitleSearch', false);
    this.page.skip = 0;
    this.fetch();
  }

  currentPageDataChange(params: NzTableQueryParams): void {
    const { pageSize: limit, pageIndex: skip, filter, sort } = params;
    
    this.page.skip = skip - 1;
    this.page.limit = limit;

    this.query.modifiedQuery({ filter, sort });

    this.fetch();
  }

  navigate(id: string): void {
    this.router.navigate(['/excel', id]);
  }

  confirm(id?: string, title?: string): void {
    let message: string;
    let ids: string[];

    if(id) {
      message  = `удалить таблицу "${title}"`
      ids = [id];
    } else {
      message = 'удалить таблицы';
      ids = this.viewService.getState('selectedTables')
    }

    this.confirmService.option('[DASHBOARD] remove-tables', message, ids);
  }
}
