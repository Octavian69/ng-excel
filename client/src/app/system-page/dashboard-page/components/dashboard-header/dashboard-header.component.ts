import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { TVisibility } from 'src/app/shared/types/shared.types';
import { ANFadeState } from 'src/app/shared/animations/animations';

@Component({
  selector: 'excel-dashboard-header',
  templateUrl: './dashboard-header.component.html',
  styleUrls: ['./dashboard-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANFadeState()]
})
export class DashboardHeaderComponent {

  @Input() isUploadReq: boolean = false;
  @Input() isShowUpload: boolean = false;
  @Input() avatar: string;
  @Output('logout') _logout = new EventEmitter<void>();
  @Output('remove') _remove = new EventEmitter<void>();
  @Output('showUpload') _showUpload = new EventEmitter<TVisibility>();
  @Output('upload') _upload = new EventEmitter<File>();

  getAvatarTooltip(): string {
    const action: string = this.avatar ? 'Изменить' : 'Установить';
    
    return `${action} аватар`; 
  }

  logout(): void {
    this._logout.emit();
  }

  remove(): void {
    this._remove.emit();
  }

  showUpload(type: TVisibility): void {
    this._showUpload.emit(type)
  }

  upload(file: File): void {
    this._upload.emit(file);
  }
}
