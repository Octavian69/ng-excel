import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { DashboardViewService } from '../../services/dashboard.view.service';
import { DashboardState } from '../../state/DashboardState';
import { DashboardDataFlags } from '../../flags/dashboard.data.flags';
import { DashboardDataService } from '../../services/dashboard.data.service';
import { LoginService } from 'src/app/auth-page/services/login.service';
import { DashboardViewFlags } from '../../flags/dashboard.view.flags';
import { OnInitMetadata } from 'src/app/shared/interfaces/hooks/OnInitMetadata';
import { UserDataService } from 'src/app/shared/user/services/user.data.service';
import { UserDataFlags } from 'src/app/shared/user/flags/user.data.flags';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/shared/user/interfaces/IUser';
import { map, filter, takeUntil } from 'rxjs/operators';
import { ITable } from 'src/app/system-page/interfaces/ITable';
import { PaginationData } from 'src/app/shared/models/services/PaginationData';
import { ConfirmService } from 'src/app/shared/confirm/services/confirm.service';
import { TUserAction } from 'src/app/shared/user/types/user.types';
import { OnInitEmitterStream } from 'src/app/shared/interfaces/hooks/OnInitEmitterStream';
import { OnInitConfirmStream } from 'src/app/shared/interfaces/hooks/OnInitConfirmStream';
import { UnsubscribeManager } from 'src/app/shared/managers/models/UnsubscribeManager';

@Component({
  selector: 'excel-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardPageComponent extends UnsubscribeManager implements 
  OnInit, 
  OnInitMetadata, 
  OnInitConfirmStream,
  OnInitEmitterStream,
  OnDestroy
{

  avatar$: Observable<string> = this.getAvatarStream();
  tables$: Observable<PaginationData<ITable>> = this.dataService.getStream('tables$');

  userDataFlags: UserDataFlags;
  dataFlags: DashboardDataFlags;
  viewFlags: DashboardViewFlags;
  state: DashboardState;

  constructor(
    private dataService: DashboardDataService,
    public userDataService: UserDataService,
    public viewService: DashboardViewService,
    public confirmService: ConfirmService<TUserAction>,
    public loginService: LoginService,
    private cdr: ChangeDetectorRef
  ) { super() }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {
    this.dataService.destroy();
    this.viewService.destroy(['flags', 'state']);
    this.unsubscribe();
  }

  ngOnInitEmiterStream(): void {
    this.initUserActionStream();
  }

  ngOnInitMetadata() {
    this.userDataFlags = this.userDataService.getFullFlags();
    this.dataFlags = this.dataService.getFullFlags();
    this.viewFlags = this.viewService.getFullFlags();
    this.state = this.viewService.getFullState();
  }

  initUserActionStream(): void {
    const stream$ = this.userDataService.getEmitterStream();

    stream$.pipe(
      takeUntil(this.untilDestoyed())
    )
    .subscribe(({ type }) => {
      switch(type) {
        case '[USER] remove': {
          this.confirmService.loading(false);
          this.loginService.logout();
          this.userDataService.destroy();
          break;
        }
        case '[USER] upload-avatar': {
          this.viewService.setFlag('isShowUpload', false);
          break;
        }
      }

      this.cdr.detectChanges();
    })
  }

  ngOnInitConfirmStream(): void {
    this.confirmService.confirmStream().pipe(
      takeUntil(this.untilDestoyed())
    )
    .subscribe(({type, value}) => {
      switch(type) {
        case '[USER] remove': {
          if(value) {
            this.confirmService.loading();
            this.userDataService.remove();
          } else {
            this.confirmService.loading(false);
          }
        }
      }
    })
  }

  init(): void {
    this.ngOnInitMetadata();
    this.ngOnInitEmiterStream();
    this.ngOnInitConfirmStream();
  }

  getAvatarStream(): Observable<string> {
    return this.userDataService.getStream<IUser>('user$')
    .pipe(
      filter(Boolean),
      map(({ Avatar }) => Avatar)
    );
  }

  confirmRemoveUser(): void {
    this.confirmService.option('[USER] remove', 'удалить свой личный кабинет');
  }
}
