import { ITable } from '../../interfaces/ITable';
import { IFilter } from 'src/app/shared/interfaces/services/IFilter';

export type TDashboardAction  = '[DASHBOARD] remove-tables';
export type TDashboardTable = 'create' | 'default';

export type TDashboardFilters = Pick<ITable, 'Title'>;
export type TDashboardSorted = Pick<ITable, 'LastVisit'>;
export type TDashboardQuery = IFilter<TDashboardFilters, TDashboardSorted>;