import { PaginationData } from 'src/app/shared/models/services/PaginationData';

export const DashboardDB = {
    services: {
        defaultStreams: [
            { stream: 'tables$', value: new PaginationData }
        ]
    },
    table: {
        defaultQuery: {
            filter: { Title: '' },
            sort: { LastVisit: -1 }
        }
    }
}

export type TDashboardDB = typeof DashboardDB;