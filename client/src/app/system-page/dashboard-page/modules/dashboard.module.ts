import { NgModule } from '@angular/core';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from 'src/app/shared/modules/shared.module';

import { DashboardViewService } from '../services/dashboard.view.service';

import { DashboardPageComponent } from '../components/dashboard-page/dashboard-page.component';
import { DashboardListCardComponent } from '../components/dashboard-list-card/dashboard-list-card.component';
import { DashboardListComponent } from '../components/dashboard-list/dashboard-list.component';
import { DashboardTableComponent } from '../components/dashboard-table/dashboard-table.component';
import { DashboardHeaderComponent } from '../components/dashboard-header/dashboard-header.component';
import { DashboardDataService } from '../services/dashboard.data.service';

@NgModule({
    declarations: [
        DashboardPageComponent,
        DashboardHeaderComponent,
        DashboardListComponent,
        DashboardListCardComponent,
        DashboardTableComponent
    ],
    imports: [
        SharedModule,
        DashboardRoutingModule
    ],
    providers: [
        DashboardDataService,
        DashboardViewService,
    ]
})
export class DashboardModule {}