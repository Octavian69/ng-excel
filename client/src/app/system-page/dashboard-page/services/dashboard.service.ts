import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServicesModule } from 'src/app/shared/modules/services.module';
import { ITable } from '../../interfaces/ITable';
import { IMessage } from 'src/app/shared/interfaces/services/IMessage';
import { Page } from '../../models/Page';
import { IPaginationData } from 'src/app/shared/interfaces/services/IPaginationData';
import { TDashboardQuery } from '../types/dashboard.types';

@Injectable({
    providedIn: ServicesModule
})
export class DashboardService {
    constructor(
        private http: HttpClient
    ) {}

    fetch(page: Page, query: TDashboardQuery): Observable<IPaginationData<ITable>> {
        return this.http.post<IPaginationData<ITable>>('@/dashboard/fetch', { page, ...query });
    }

    remove(body: string[]): Observable<IMessage> {
        const headers: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
        });
        const httpOptions = { headers, body };

        return this.http.delete<IMessage>('@/dashboard/remove', httpOptions);
    }
}