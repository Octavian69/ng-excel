import { Injectable } from '@angular/core';
import { DashboardViewFlags } from '../flags/dashboard.view.flags';
import { ITable } from '../../interfaces/ITable';
import { DashboardState } from '../state/DashboardState';
import { TVisibility } from 'src/app/shared/types/shared.types';
import { DashboardDB, TDashboardDB } from '../db/dashboard.db';
import { ViewServiceManager } from 'src/app/shared/managers/models/ViewServiceManager';

@Injectable()
export class DashboardViewService extends ViewServiceManager<DashboardViewFlags, DashboardState, TDashboardDB> {

    constructor(
    ) {
        super(new DashboardViewFlags, new DashboardState, DashboardDB);
    }

    showUpload(type: TVisibility): void {
        const flag: boolean = type === 'hide' ? false : true;
        this.setFlag('isShowUpload', flag);
    }

    select(id: string): void {
        const selectedTables: string[] = this.getState('selectedTables');
        const idx: number = selectedTables.indexOf(id);

        if(~idx) {
            selectedTables.splice(idx, 1);
        } else {
            selectedTables.push(id);
        }

        this.setState('selectedTables', selectedTables);
    }

    selectAll(value: boolean, rows: ITable[]): void {

        const selectedTables: string[] = [];

        if(value) {
            const ids: string[] = rows.map(t => t._id);
    
            selectedTables.push(...ids);
        } 

        this.setState('selectedTables', selectedTables);
    }
}