import { DashboardDB, TDashboardDB } from '../db/dashboard.db'
import { DashboardService } from './dashboard.service';
import { IPaginationData } from 'src/app/shared/interfaces/services/IPaginationData';
import { ITable } from '../../interfaces/ITable';
import { takeUntil, debounceTime, switchMap, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { NSTOASTR } from 'src/app/shared/namespaces/shared.namespaces';
import { IMessage } from 'src/app/shared/interfaces/services/IMessage';
import { DashboardDataFlags } from '../flags/dashboard.data.flags';
import { BehaviorSubject, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { IPaginationOptions } from 'src/app/shared/interfaces/services/IPaginationOptions';
import { TDefaultStreamValue } from 'src/app/shared/types/services.types';
import { PaginationData } from 'src/app/shared/models/services/PaginationData';
import { TDashboardAction, TDashboardQuery } from '../types/dashboard.types';
import { DataServiceManager } from 'src/app/shared/managers/models/DataServiceManager';

@Injectable()
export class DashboardDataService extends DataServiceManager<DashboardDataFlags, TDashboardAction, TDashboardDB> {

    tables$: BehaviorSubject<IPaginationData<ITable>> = new BehaviorSubject(new PaginationData);
    fetch$: Subject<IPaginationOptions<TDashboardQuery>> = new Subject();

    constructor(
        private toastr: ToastrService,
        private dashboardService: DashboardService,
    ) {
        super(new DashboardDataFlags, DashboardDB);
    }

    fetch(): void {
        this.fetch$.pipe(
            tap(_ => this.setFlag('isFetchReq', true)),
            debounceTime(300),
            switchMap((action: IPaginationOptions<TDashboardQuery>) => {
                const { page, query } = action;
                return this.dashboardService.fetch(page, query);
            }),
            takeUntil(this.untilDestoyed())
        ).subscribe((tables: IPaginationData<ITable>) => {
            this.next('tables$', tables)
            this.setFlag('isFetchReq', false);
        });
    }

    remove(ids: string[]): void {
        this.setFlag('isRemoveReq', true);

        const next = ({ message }: IMessage) => {
            const { rows, totalCount } = this.getStreamValue('tables$');

            const updateRows: ITable[] = rows.filter(t => !ids.includes(t._id));
            const updateCount: number = totalCount - ids.length;
            const updateTables = new PaginationData(updateRows, updateCount)
            this.next('tables$', updateTables);
            this.setFlag('isRemoveReq', false);
            this.emit('[DASHBOARD] remove-tables', {ids, rowsLength: updateRows.length} )
            this.toastr.success(message, NSTOASTR.SUCCESS);
        }

        this.dashboardService.remove(ids).pipe(
            takeUntil(this.untilDestoyed())
        ).subscribe({ next });
    }

    destroy(): void {
        const defaultValues: TDefaultStreamValue[] = this.getDataFromDB(['services', 'defaultStreams']);
        super.destroy(defaultValues, ['flags']);
    }
}