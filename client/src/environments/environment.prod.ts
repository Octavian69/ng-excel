import { IEnvironment } from './interfaces/IEnvironment';

export const environment: IEnvironment = {
  production: true,
  API_URL: 'https://angular-excel.herokuapp.com'
};
