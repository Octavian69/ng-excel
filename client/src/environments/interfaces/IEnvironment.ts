export interface IEnvironment {
    production: boolean;
    API_URL: string;
}