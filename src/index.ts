import * as mongoose from 'mongoose';
import * as config from 'config';
import { app } from './modules/app/app';

const PORT = process.env.PORT || config.get('PORT');
const MONGO_URI: string = config.get('MONGO_URI');

function start() {
    try {
        mongoose.connect(MONGO_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        }).then(_ => {
            console.log(`MongoDB has been started`);
        });

        app.listen(PORT, () => {
            console.log(`Server has been started on port ${PORT}`);
        });
        
    } catch(e) {
        console.error(e);
    }
}

start();