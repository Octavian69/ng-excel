export const AuthDB = {
    foldersPath: ['src', 'uploads'],
    accessDeniedMessage: 'Неправильный логин или пароль',
    tokenErrorMessage: 'Используйте данные учетной записи для входа в кабинет.',
    whiteList: ['user', 'dashboard', 'excel']
}
