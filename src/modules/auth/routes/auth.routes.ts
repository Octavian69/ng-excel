import { Router } from 'express';
import { login, registration, getUserByEmail, refreshTokens } from '../controllers/auth.controllers';

const router: Router = Router();

router.post('/login', login);
router.post('/registration', registration);
router.post('/get-by-email', getUserByEmail);
router.put('/refresh-tokens', refreshTokens);


export { router as AuthRouter};