import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import * as config from 'config';
import { errorHandler } from "../../app/handlers/error.handler";
import { IRequest } from "../../app/interfaces/IRequest";
import { IResponse } from "../../app/interfaces/IResponse";
import { IMessage } from "../../app/interfaces/IMessage";
import { IAuthTokens } from "../interfaces/IAuthTokens";
import { createUserFolders, generateTokens } from '../handlers/auth.handlers';
import { getDataFromDB } from '../../app/handlers/shared.handlers';
import { AuthDB } from '../db/auth.db';
import { TokensModel } from '../models/Tokens.model';
import { IUser } from '../../user/interfaces/IUser';
import { UserModel } from '../../user/models/User.model';

export async function login(req: IRequest<Pick<IUser, 'Email' | 'Password'>>, res: IResponse<IMessage | IAuthTokens>) {
    try {
        const { Email, Password } = req.body;

        const user: IUser = await UserModel.findOne({ Email });
        const errorMessage: string = getDataFromDB(AuthDB, ['accessDeniedMessage']);

        if(!user) {
            return res.status(404).json({ message: errorMessage })
        }

        const isValidPassword: boolean = bcrypt.compareSync(Password, user.Password);

        if(!isValidPassword) {
            return res.status(404).json({ message: errorMessage })
        }

        const { _id } = user;

        const $set: Partial<IAuthTokens> = generateTokens({ Email, _id });
        const tokens: IAuthTokens = await TokensModel.findOneAndUpdate(
            { User: _id },
            { $set },
            { new: true }
        );

        res.status(200).json(tokens);
    } catch(e) {
        errorHandler(e, res);
    }
}

export async function registration(req: IRequest<Exclude<IUser, '_id'>>, res: IResponse<IMessage>) {
    try {
        const salt: number = config.get('PASSWORD_SALT');
        const Password: string = bcrypt.hashSync(req.body.Password, salt);
        const candidate = Object.assign(req.body, { Password });
        const user: IUser = await new UserModel(candidate).save();

        await new TokensModel({ User: user._id }).save();

        createUserFolders(String(user._id));

        res.status(200).json({ message: 'Используйте свои данные для входа в кабинет.' });
    } catch(e) {
        errorHandler(e, res);
    }
}

export async function getUserByEmail(req: IRequest<Pick<IUser, 'Email'>>, res: IResponse<IUser>) {
    try {
        const { Email } = req.body;
        const user: IUser = await UserModel.findOne({ Email });

        res.status(200).json(user);

    } catch(e) {
        errorHandler(e, res);
    }
}

export async function refreshTokens(req: IRequest<{ refreshToken: string }>, res: IResponse<IAuthTokens | IMessage>) {
    try {
        const { refreshToken } = req.body;
        const { _id, Email } = jwt.decode(refreshToken, config.get('JWT_REFRESH'));
        const tokens: IAuthTokens = await TokensModel.findOne({ User: _id });
        const isNotEqual: boolean = !Object.is(refreshToken, tokens.refresh_token);

        if(isNotEqual) {
            const message: string = getDataFromDB(AuthDB, ['tokenErrorMessage']);
            return res.status(401).json({ message });
        }

        const candidateTokens: Partial<IAuthTokens> = generateTokens({ _id, Email });

        Object.assign(tokens, candidateTokens);

        await tokens.save();

        res.status(200).json(tokens);
        
    } catch(e) {
        errorHandler(e, res);
    }
}




