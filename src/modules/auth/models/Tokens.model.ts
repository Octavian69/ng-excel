import { Schema, model, Types } from 'mongoose';
import { IAuthTokens } from '../interfaces/IAuthTokens';
const TokensSchema = new Schema({
    access_token: {
        type: String,
        default: null
    },
    refresh_token: {
        type: String,
        default: null
    },
    User: {
        refs: 'users',
        type: Types.ObjectId,
        unique: true
    }
});

export const TokensModel = model<IAuthTokens>('tokens', TokensSchema);

