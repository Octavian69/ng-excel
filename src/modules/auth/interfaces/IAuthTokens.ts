import { Document } from "mongoose";

export interface IAuthTokens extends Document {
    access_token: string;
    refresh_token: string;
}