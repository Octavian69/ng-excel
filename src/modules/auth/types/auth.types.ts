import { IUser } from "../../user/interfaces/IUser";

export type TRequestUser = Pick<IUser, 'Email' | '_id'>;