import * as path from 'path';
import * as fs from 'fs';
import * as jwt from 'jsonwebtoken';
import * as config from 'config';
import * as rimraf from 'rimraf';
import { getDataFromDB } from '../../app/handlers/shared.handlers';
import { AuthDB } from '../db/auth.db';
import { IAuthTokens } from '../interfaces/IAuthTokens';
import { IUser } from '../../user/interfaces/IUser';

export function createUserFolders(userId: string): void {
    const foldersPath: string[] = getDataFromDB(AuthDB, ['foldersPath']);
    const userPath: string = path.resolve(...foldersPath, userId);

    fs.mkdirSync(userPath);
}

export function removeUserFolders(userId: string): void {
    const foldersPath: string[] = getDataFromDB(AuthDB, ['foldersPath']);
    const userPath: string = path.resolve(...foldersPath, userId);

    rimraf.sync(userPath)
}

export function generateTokens(data: Pick<IUser, 'Email' | '_id'>): Partial<IAuthTokens> {
    const secret_access: string = config.get('JWT_KEY');
    const secret_refresh: string = config.get('JWT_REFRESH');
    const access_token = jwt.sign(data, secret_access, { expiresIn: '1h' });
    const refresh_token = jwt.sign(data, secret_refresh, { expiresIn: '7d' });

    return { access_token, refresh_token };
}
