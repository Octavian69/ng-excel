import * as config from 'config';
import * as jwt from "jsonwebtoken";
import { IRequest } from "../../app/interfaces/IRequest";
import { IResponse } from "../../app/interfaces/IResponse";
import { NextFunction } from "express";
import { getDataFromDB, prefix } from "../../app/handlers/shared.handlers";
import { AuthDB } from "../db/auth.db";
import { TokensModel } from '../models/Tokens.model'
import { IAuthTokens } from '../interfaces/IAuthTokens';
import { TRequestUser } from '../types/auth.types';
import { IMessage } from '../../app/interfaces/IMessage';

export function authenticate() {
    const { JsonWebTokenError, NotBeforeError, TokenExpiredError } = jwt;
    const message: string = getDataFromDB(AuthDB, ['tokenErrorMessage']);

    return async (req: IRequest, res: IResponse<IMessage>, next: NextFunction) => {
        if(isNext(req.url, req.method)) return next();

        const token: string = sliceToken(req.headers.authorization);
        const decodedToken = jwt.decode(token) as TRequestUser;

        jwt.verify(token, config.get('JWT_KEY'), async (err) => {

            const { access_token }: IAuthTokens = await TokensModel.findOne({ User: decodedToken._id });
            const isNotEqual: boolean = !Object.is(access_token, token);
            
            switch(true) {
                case err instanceof TokenExpiredError: {
                     return res.status(400).json();
                }
                case isNotEqual:
                case err instanceof NotBeforeError: 
                case err instanceof JsonWebTokenError: {
                    return res.status(401).json({message});
                }
            }

            req.user = decodedToken;
            next();
        })
    }
}

function isNext(url: string, method: string): boolean {
    const tokenUrls: string[] = getDataFromDB(AuthDB, ['whiteList']);
    const isOptions: boolean = Object.is(method, 'OPTIONS');

    return !tokenUrls.some(path => url.includes(prefix(path, 'api/'))) || isOptions;
}

function sliceToken(token: string): string {
    const authSchema: string = config.get('AUTH_SCHEMA');

    return token?.replace(authSchema, '');
}