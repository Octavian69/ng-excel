import { ITable } from "../../excel/interfaces/ITable";
import { IRequestQuery } from "../../app/interfaces/IRequestQuery";

export type TDashboardFilter = IRequestQuery<Pick<ITable, 'Title' | 'User'>, ITable>; 