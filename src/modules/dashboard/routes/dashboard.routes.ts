import { Router } from 'express';
import { fetch, remove } from '../controllers/dashboard.controllers';

const router: Router = Router();

router.post('/fetch', fetch);
router.delete('/remove', remove);

export { router as DashboardRouter };