import { mongoEqual, mongoRegex } from "../../app/handlers/app.mongo.handers"

export const DashboardDB = {
    filters: {
        fetch: {
            Title: (value: string) => mongoRegex(value),
            User: (userId: string) => mongoEqual(userId, true)
        }
    }
};