import { IRequest } from "../../app/interfaces/IRequest";
import { IResponse } from "../../app/interfaces/IResponse";
import { IMessage } from "../../app/interfaces/IMessage";
import { errorHandler } from "../../app/handlers/error.handler";
import { IPaginationData } from "../../app/interfaces/IPaginationData";
import { ITable } from "../../excel/interfaces/ITable";
import { TDashboardFilter } from "../types/dashboard.types";
import { completeRequestQuery } from "../../app/handlers/app.mongo.handers";
import { DashboardDB } from "../db/dashboard.db";
import { TableModel } from "../../excel/models/Table.model";


export async function fetch(req: IRequest<TDashboardFilter>, res: IResponse<IPaginationData<ITable>>) {
    try {
        req.body.filter.User = req.user._id;

        const aggregate: Array<object> = completeRequestQuery(DashboardDB, ['filters', 'fetch'], req.body);
        const [result] = await TableModel.aggregate(aggregate);
        const rows: ITable[] = result?.rows || [];
        const totalCount: number = result?.totalCount || 0;

        res.status(200).json({ rows, totalCount });

    } catch(e) {
        errorHandler(e, res)
    }
}

export async function remove(req: IRequest<string[]>, res: IResponse<IMessage>) {
    try {
        let message: string;

        if(req.body.length > 1) {
            await TableModel.deleteMany({ _id: { $in: req.body } });
            message = 'Таблицы удалены.'
        } else {
            const [id] = req.body;
            const table: ITable = await TableModel.findByIdAndDelete(id);

            message = `Таблица "${table.Title}" удалена.`
        }

        res.status(200).json({ message });

    } catch(e) {
        errorHandler(e, res);
    }
}