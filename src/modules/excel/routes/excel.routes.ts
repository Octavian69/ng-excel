import { Router } from 'express';
const router: Router = Router();

import { 
    fetch, create,
    edit, remove
} from '../controllers/excel.controllers';

router.get('/get-by-id/:id', fetch);
router.post('/create', create);
router.patch('/edit/:id', edit);
router.delete('/remove/:id', remove);

export { router as ExcelRouter };