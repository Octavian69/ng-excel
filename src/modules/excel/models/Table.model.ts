import { Schema, model, Types } from 'mongoose';
import { ITable } from '../interfaces/ITable';

const TableSchema = new Schema({
    Title: {
        type: String,
        default: 'Новая таблица'
    },
    Created: {
        type: Date,
        default: Date.now
    },
    LastVisit: {
        type: Date,
        default: Date.now
    },
    CellContent: {
        type: Schema.Types.Mixed,
        default: {}
    },
    CellStyles: {
        type: Schema.Types.Mixed,
        default: {}
    },
    ColSize: {
        type: Schema.Types.Mixed,
        default: {},
    },
    RowSize: {
        type: Schema.Types.Mixed,
        default: {}
    },
    User: {
        refs: 'users',
        type: Types.ObjectId,
        required: true
    }
}, { minimize: false })

export const TableModel = model<ITable>('table', TableSchema);