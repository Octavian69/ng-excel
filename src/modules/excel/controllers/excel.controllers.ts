import { IRequest } from '../../app/interfaces/IRequest';
import { IResponse } from '../../app/interfaces/IResponse';
import { IMessage } from '../../app/interfaces/IMessage';
import { ITable } from '../interfaces/ITable';
import { TableModel } from '../models/Table.model';
import { errorHandler } from '../../app/handlers/error.handler';

export async function fetch(req: IRequest, res: IResponse<ITable>) {
    try {
        const { id } = req.params;
        const table: ITable = await TableModel.findByIdAndUpdate(id, { $set: { LastVisit: new Date } }, { new: true });

        res.status(200).json(table)

    } catch(e) {
        errorHandler(e, res);
    }
}

export async function create(req: IRequest<ITable>, res: IResponse<ITable>) {
    try {
        const candidate: Partial<ITable> = Object.assign(
            {}, req.body, { User: req.user._id }
        );

        const table: ITable = await new TableModel(candidate).save();

        res.status(200).json(table);

    } catch(e) {

        console.log(e)
        errorHandler(e, res);
    }
}

export async function edit(req: IRequest<ITable>, res: IResponse<ITable>) {
    try {

        const candidate = Object.assign({}, req.body, { LastVisit: new Date })
        const table: ITable = await TableModel.findByIdAndUpdate(
            req.params.id, { $set: candidate }, { new: true }
        );

        res.status(200).json(table);

    } catch(e) {
        errorHandler(e, res);
    }
}

export async function remove(req: IRequest, res: IResponse<IMessage>) {
    try {

        const tabel: ITable = await TableModel.findByIdAndDelete(req.params.id);
        
        res.status(200).json({ message: `Таблица "${tabel.Title}" удалена.` });

    } catch(e) {
        errorHandler(e, res);
    }
}