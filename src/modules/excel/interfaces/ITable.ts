import { Document } from "mongoose";
import { ISimple } from "../../app/interfaces/ISimple";

export interface ITable extends Document {
    Title: string;
    Created: Date;
    LastVisit: Date;
    CellContent: ISimple<string>;
    CellStyles: ISimple<ISimple<string>>;
    ColSize: ISimple<number>;
    RowSize: ISimple<number>;
    User: string;
    _id: string;
}