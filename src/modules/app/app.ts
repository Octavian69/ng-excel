import * as express from 'express';
import * as morgan from 'morgan';
import * as cors from 'cors';
import { initAppRoutes } from './handlers/app.routes.handlers';
import { authenticate } from '../auth/handlers/authenticate'

const app: express.Application = express();

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors({origin: '*'}));
app.use(authenticate());
initAppRoutes(app);


export { app };
