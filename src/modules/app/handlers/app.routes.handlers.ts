import * as express from 'express';
import * as PATH from 'path';
import * as fs from 'fs';
import { IRouteItem } from "../interfaces/IRouteItem";
import { AuthRouter } from "../../auth/routes/auth.routes";
import { UserRouter } from "../../user/routes/user.routes";
import { DashboardRouter } from "../../dashboard/routes/dashboard.routes";
import { ExcelRouter } from '../../excel/routes/excel.routes';

const routes: IRouteItem[] = [
    { path: '/src/uploads', router: express.static('src/uploads') },
    { path: '/api/auth', router: AuthRouter },
    { path: '/api/user', router: UserRouter },
    { path: '/api/dashboard', router: DashboardRouter },
    { path: '/api/excel', router: ExcelRouter }
];

function initBuildRoutes(app: express.Application): void {
    app.use(express.static('client/dist/excel'));

    app.get('*', (req: express.Request, res: express.Response) => {
        res.sendFile(
            PATH.resolve(__dirname, '../../../../', 'client', 'dist', 'excel', 'index.html')
        )
    })
}


export function initAppRoutes(app: express.Application): void {
    routes.forEach(({ path, router }) => app.use(path, router));

    if(process.env.NODE_ENV === 'production') {
        initBuildRoutes(app);
    }
}

