import { Types } from "mongoose";
import { IRequestQuery } from "../interfaces/IRequestQuery";
import { TFilterHandlers } from "../types/app.request.types";
import { ISimple } from "../interfaces/ISimple";
import { getDataFromDB } from "./shared.handlers";


export function toObjectId(id: string) {
    return Types.ObjectId(id);
}

export function objectIdToString(id: Types.ObjectId) {
    return String(id);
}

export function completeRequestQuery(
    db: any, path: string[],
    requestQuery: IRequestQuery<any, any>,
    isPagination: boolean = true
): Array<object>
{
    const filterHandlers: TFilterHandlers = getDataFromDB(db, path, false);
    const { filter = {}, sort, page: { skip, limit } } = requestQuery;

    const query: ISimple<any> = Object.entries(filter).reduce((accum, [key, value]) => {
        if(value) {
            accum.$match[key] = filterHandlers[key](value);
        }

        return accum;
    }, { $match: {} });

    const aggregate: Array<object> = [query];

    if(sort) aggregate.push({ $sort: sort });
    if(isPagination) aggregate.push(...paginationAggregate(skip, limit));

    return aggregate;
}

export function paginationAggregate(skip: number, limit: number): Array<object> {
return [
    { $group: {
        _id: null,
        totalCount: { $sum: 1 },
        results: { $push: '$$ROOT' }
    } },
    
    { $project: {
        totalCount: 1,
        rows: { $slice: ['$results', skip * limit, +limit] }
    } }
]
};

//query

export function mongoRegex(value, flags: string = 'gi') {
    return { $regex: new RegExp(value, flags) };
}


export function mongoEqual(value: any, isObjectId: boolean = false) {
    return { $eq: isObjectId ? toObjectId(value) : value  }
}