export function deepCopy<T>(value: T): T {
    return JSON.parse(JSON.stringify(value))
}

export function getDataFromDB<T>(db: any, path: string[] = [], isCopy: boolean = true): T {
    let value = isCopy ? deepCopy(db) : db;
    path = path.concat();

    while(path.length) {
        value = value[path[0]];
        path.shift();

        if(!value) return null;
    }

    return isCopy ? deepCopy<T>(value) : value;
}

export function prefix(str: string, val: string | number): string {
    return val + str;
}

export function suffix(str: string, val: string | number): string {
    return str + val;
}

