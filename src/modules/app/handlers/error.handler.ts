import { Response } from 'express';

export function errorHandler(error: Error, res: Response): void {
    res.status(500).json(error);
}