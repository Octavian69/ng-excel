import { ISimple } from "../interfaces/ISimple"

export type TSort<T> = {
    [key in keyof Partial<T>]: number;
}

export type TFilterHandlers = {
    [key: string]: (v: any) => ISimple<any>
}