import { IPage } from "./IPage";
import { IFilter } from "./IFilter";

export interface IRequestQuery<F extends object = null, S extends object = null> extends IFilter<F, S> {
    page: IPage;
}