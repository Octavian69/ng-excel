export interface IPaginationData<T> {
    rows: T[],
    totalCount: number
}