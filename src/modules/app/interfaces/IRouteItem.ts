import { Router, Handler } from "express";

export interface IRouteItem {
    path: string;
    router: Router | Handler
}