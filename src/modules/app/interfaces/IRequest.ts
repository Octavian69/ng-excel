import { Request } from 'express';
import { TRequestUser } from '../../auth/types/auth.types';

export interface IRequest<T = null> extends Request {
    body: T;
    user?: TRequestUser
}
