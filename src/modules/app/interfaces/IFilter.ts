import { TSort } from "../types/app.request.types";

export interface IFilter<F extends object, S extends object> {
    filter: F;
    sort: TSort<S>
}