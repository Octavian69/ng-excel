import { Response } from 'express';

export interface IResponse<T = null> extends Response<T> {}