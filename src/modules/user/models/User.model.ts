import * as mongoose from 'mongoose';
import { IUser } from '../interfaces/IUser';
const  { Schema, model } = mongoose;

const UserSchema = new Schema({
    Email: {
        type: String,
        required: true,
        unique: true
    },
    Password: {
        type: String,
        required: true
    },
    Created: {
        type: Date,
        default: Date.now
    },
    Avatar: {
        type: String,
        default: null
    }
});

export const UserModel = model<IUser>('users', UserSchema);