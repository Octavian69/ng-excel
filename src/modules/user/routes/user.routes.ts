import * as multer from 'multer';
import { Router } from 'express';
import { getUserById, uploadAvatar, remove } from '../controllers/user.controllers';
import { UserStorage } from '../storage/user.storage';

const router = Router();

router.get('/get-by-id/:id', getUserById);
router.put('/upload-avatar', multer({ storage: UserStorage }).single('avatar'), uploadAvatar);
router.delete('/remove/:id', remove);

export { router as UserRouter };