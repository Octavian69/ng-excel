import { Document } from 'mongoose';

export interface IUser extends Document {
    Email: string;
    Password: string;
    Created: Date;
    Avatar: string;
    _id: string;
}