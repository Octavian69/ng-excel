import * as multer from 'multer';
import * as rimraf from 'rimraf';
import * as Path from 'path';

import { suffix } from '../../app/handlers/shared.handlers';
import { IRequest } from '../../app/interfaces/IRequest';
import { TRequestUser } from '../../auth/types/auth.types';
import { UserModel } from '../models/User.model';
import { IUser } from '../interfaces/IUser';

const storage = multer.diskStorage({
    async destination(req: IRequest<TRequestUser>, file, cb) {
        const { Avatar }: IUser = await UserModel.findById(req.user._id);

        if(Avatar) rimraf.sync(Path.resolve(Avatar));

        const path: string = suffix('src/uploads/', req.user._id);
        
        cb(null, path)
    },
    filename(req, file, cb) {
        cb(null, file.originalname);
    }
});

export { storage as UserStorage };