import { IRequest } from "../../app/interfaces/IRequest";
import { IResponse } from "../../app/interfaces/IResponse";
import { IUser } from "../interfaces/IUser";
import { UserModel } from "../models/User.model";
import { TableModel } from "../../excel/models/Table.model";
import { errorHandler } from "../../app/handlers/error.handler";
import { removeUserFolders } from "../../auth/handlers/auth.handlers";
import { TokensModel } from "../../auth/models/Tokens.model";
import { IMessage } from "../../app/interfaces/IMessage";

export async function getUserById(req: IRequest, res: IResponse<IUser>) {
    try {
        const user: IUser = await UserModel.findById(req.params.id);

        res.status(200).json(user);

    } catch(e) {
        errorHandler(e, res)
    }
}

export async function uploadAvatar(req: IRequest, res: IResponse<IUser>) {
    try {
        const { path } = req.file;
        const user: IUser = await UserModel.findByIdAndUpdate(
            req.user._id, 
            { $set: { Avatar: path } },
            { new: true }
        );

        res.status(200).json(user);

    } catch(e) {
        errorHandler(e, res)
    }
}

export async function remove(req: IRequest, res: IResponse<IMessage>) {
    try {
        const _id = req.params.id;

        await UserModel.findOneAndDelete({ _id });
        await TokensModel.findOneAndDelete({ User: _id });
        await TableModel.deleteMany({ User: _id });

        removeUserFolders(_id);

        res.status(200).json({ message: 'Ваш личный кабинет удален.' })

    } catch(e) {
        errorHandler(e, res);
    }
}

 